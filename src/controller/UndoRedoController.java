package controller;

import model.UndoRedoModel;


public class UndoRedoController {

	
	private UndoRedoModel undoRedoModel;
	
	public UndoRedoController() {
		
		undoRedoModel = new UndoRedoModel();
	} 
	
	public UndoRedoModel getUndoRedoModel() {
		return this.undoRedoModel;
	}
	
	
}
