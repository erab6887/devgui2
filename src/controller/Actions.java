package controller;

import helpers.AnimationEngine;
import helpers.CalendarHelper;
import helpers.Settings;
import helpers.p;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import model.TaskModel;
import model.XmlTaskModel;

import org.joda.time.LocalDate;

import view.CalendarPopup;
import view.CalendarView;
import view.CustomGlassPane;
import view.Menu;
import view.components.DayCard;
import view.components.EventCard;
import view.components.easteregg.BezierPath;

import com.sun.awt.AWTUtilities;

public class Actions implements MouseListener, MouseMotionListener, ActionListener {

	public  static CalendarView view;
	private XmlTaskModel model;
	private Menu menu;
	private JFrame mainFrame;
	private UndoRedoController undoRedoController;
	private CalendarPopup popup;

	private int recentTaskId;
	private int recentTaskDay;
	private String recentTaskMonth;
	private int recentTaskYear;
	private int recentTaskWeek;
	private int recentTaskStartTime;
	private int recentTaskStopTime;

	private boolean popupIsOpen = false;
	private boolean shiftPressed = false;
	private boolean zPressed = false;
	private boolean commandPressed = false;
	private boolean ctrlPressed = false;
	private boolean helpCenterClicked = false;

	private EventCard tmpEventCardToUndo = null;
	private EventCard tmpEventCardToRedo = null;

	private CustomGlassPane glassPane;
	private boolean bezier = false;
	private BezierPath bpath = new BezierPath();


	public Actions(CalendarView view, XmlTaskModel model, Menu menu, JFrame mainFrame) {

		this.model = model;
		Actions.view = view;
		this.mainFrame = mainFrame;
		undoRedoController = new UndoRedoController();
		Actions.view.addMouseListener(this);
		Actions.view.addMouseMotionListener(this);
		Actions.view.addComponentListener(new WindowsResize());
		Actions.view.addKeyListener(new KeyEvents());
		this.menu = menu;
		this.menu.getMenuZeroPercent().addActionListener(new MenuEventHandler());
		this.menu.getMenuTwentyfivePercent().addActionListener(new MenuEventHandler());	
		this.menu.getMenuFiftyPercent().addActionListener(new MenuEventHandler());
		this.menu.getMenuSeventyfivePercent().addActionListener(new MenuEventHandler());
		this.menu.getMenuAbout().addActionListener(new MenuEventHandler());
		this.menu.getMenuQuit().addActionListener(new MenuEventHandler());
		this.menu.getMenuRedo().addActionListener(new MenuEventHandler());
		this.menu.getMenuUndo().addActionListener(new MenuEventHandler());
		this.menu.getMenuDelete().addActionListener(new MenuEventHandler());
		this.menu.getMenuHelpCenter().addActionListener(new MenuEventHandler());
		glassPane = new CustomGlassPane(0, 0, Settings.getFrameHeight()*0.5, Settings.getFrameWidth()*0.5);
		mainFrame.setGlassPane(glassPane);
		glassPane.setVisible(false);
	}


	private class WindowsResize extends ComponentAdapter {

		public void componentResized(ComponentEvent e) {
			// This is only called when the user releases the mouse button.
			JPanel tmp = (JPanel)e.getSource();

			Settings.setFrameHeight(tmp.getHeight());
			Settings.setFrameWidth(tmp.getWidth());        	


			Iterator<EventCard> iter = view.getEventCardsList().iterator();
			while(iter.hasNext()) {
				EventCard tmpEventCard = iter.next();
				tmpEventCard.setPosX(tmpEventCard.getParentDayCardsPosX());
			}

			view.validate();
			view.repaint();
		}

	}

	@Override
	public void mouseDragged(MouseEvent event) {
		System.out.println("mouseDragged");		

		//view.weekCard.mouseDragged(event);
		view.mouseDragged(event);
		view.repaint();

	}

	@Override
	public void mouseMoved(MouseEvent event) {		

		//view.weekCard.mouseMoved(event);

		view.getCalendarHeader().getControlPanelButton().mouseMoved(event);
		view.mouseMoved(event);
		if(Settings.isResizeMousePointer()) {
			view.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
		} else {
			view.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

	@Override
	public void mouseClicked(MouseEvent event) {
		int mouseX = event.getX();
		int mouseY = event.getY();
		if(event.getClickCount() == 2) {
			ArrayList<EventCard> eventCardsList = view.getEventCardsList();
			if(eventCardsList.size() > 0) {
				Iterator<EventCard> iter = eventCardsList.iterator();				
				while(iter.hasNext()) {
					EventCard temp = iter.next();
					if(temp.isInsideEventCard(mouseX, mouseY)) {
						if(popupIsOpen){							
							popup.dispose();
						}

						popup = new CalendarPopup(mainFrame, "Edit task", false);
						popupIsOpen = true;

						popup.setTitle(temp.getTitle());
						popup.setContent(temp.getContent());
						popup.setLocation(temp.getLocation());
						popup.setCategory(XmlTaskModel.getCategories().getCategoryModel(temp.getCategory()));
						popup.setPriority(temp.getPriority());

						popup.getSaveButton().addActionListener(this);
						recentTaskId = temp.getId();
						recentTaskDay = temp.getDay();
						recentTaskMonth = temp.getMonth()+"";
						recentTaskYear = temp.getYear();
						recentTaskWeek = temp.getWeek();
						recentTaskStartTime = temp.getStartTime();
						recentTaskStopTime = temp.getStopTime();
					}	
				}
			}

		} else {			
			ArrayList<EventCard> eventCardsList = view.getEventCardsList();
			if(eventCardsList.size() > 0) {
				Iterator<EventCard> iter = eventCardsList.iterator();				
				while(iter.hasNext()) {
					iter.next().isClicked(mouseX,mouseY);
				}
			}
		}	

	}

	@Override
	public void mouseEntered(MouseEvent event) {
		System.out.println("mouseEntered");		

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		System.out.println("mouseExited");	

	}

	@Override
	public void mousePressed(MouseEvent event) {
		System.out.println("mousePressed");	
		System.out.println("MOUSE X: "+event.getX());
		int mouseX = event.getX();
		int mouseY = event.getY();



		if(bezier) {
			Point p = new Point(mouseX,mouseY);
			view.dots.addDot(p);
			bpath.addPoint(p);

		} else {

			boolean clickedInsideControlPanel = view.getCalendarHeader().getControlPanel().isInsideControlPanel(mouseX, mouseY);
			boolean isControlPanelVisible = view.getCalendarHeader().isControlPanelVisible;
			//Undo when dragging and rezising
			if(!(clickedInsideControlPanel && isControlPanelVisible)) {



				//buttons in the header for chaning weeks
				boolean isInsideLeftButton = view.getCalendarHeader().isInsideLeftButton(mouseX, mouseY);
				boolean isInsideMiddleButton = view.getCalendarHeader().isInsideMiddleButton(mouseX, mouseY);
				boolean isInsideRightButton = view.getCalendarHeader().isInsideRightButton(mouseX, mouseY);

				if(isInsideLeftButton || isInsideMiddleButton || isInsideRightButton ) {
					if(isInsideLeftButton) {				
						CalendarHelper.minusOneWeek();			
						fetchNewTasks();			
					} else if (isInsideMiddleButton) {

						CalendarHelper.setDate(LocalDate.now());
						fetchNewTasks();
					} else if (isInsideRightButton) {				
						CalendarHelper.plusOneWeek();
						fetchNewTasks();
					}
				} else if(view.getEventCardsList().size() > 0 ) {

					Iterator<EventCard> iter = view.getEventCardsList().iterator();
					while(iter.hasNext()) {
						EventCard temp = iter.next();
						if(temp.isInsideEventCard(mouseX, mouseY) || temp.isOnBottomBorder(mouseX ,mouseY) || temp.isOnUpperBorder(mouseX ,mouseY)) {
							System.out.println("pressed on eventcard save copy undo stack");
							EventCard undoEventCard = temp.clone();
							undoRedoController.getUndoRedoModel().pushUndo(undoEventCard);
						}

					}

					if(undoRedoController.getUndoRedoModel().getUndoStack().getList().size() > 0 ) {
						System.out.println("Undo list size:" + undoRedoController.getUndoRedoModel().getUndoStack().getList().size());
						Iterator<EventCard> iterUndo = undoRedoController.getUndoRedoModel().getUndoStack().getIterator();
						while(iterUndo.hasNext()) {
							EventCard tmo = iterUndo.next();
							System.out.println("EVENTCARD: "+tmo.getId());
							System.out.println("EVENTCARD POSX: "+tmo.getPosX());
						}
					}
				}

				view.mousePressed(event);

				if(view.getCalendarHeader().getControlPanelButton().isInsideButton(mouseX, mouseY)) {
					//view.getCalendarHeader().setControlPanelRect();
					view.getCalendarHeader().isControlPanelVisible = !view.getCalendarHeader().isControlPanelVisible;

				}
			} else {
				if(view.getCalendarHeader().isControlPanelVisible) {
					System.out.println("Control panel is visible");

					if(view.getCalendarHeader().getControlPanel().getHomeButton().isInsideButton(mouseX, mouseY)) {

						boolean isButtonPressed = !view.getCalendarHeader().getControlPanel().getHomeButton().isButtonPressed();

						view.getCalendarHeader().getControlPanel().getHomeButton().setButtonPressed(isButtonPressed);

						highLightEventCards("Home");

					} else if(view.getCalendarHeader().getControlPanel().getWorkButton().isInsideButton(mouseX, mouseY)) {	

						boolean isButtonPressed = !view.getCalendarHeader().getControlPanel().getWorkButton().isButtonPressed();

						view.getCalendarHeader().getControlPanel().getWorkButton().setButtonPressed(
								isButtonPressed);

						highLightEventCards("Work");

					} else if(view.getCalendarHeader().getControlPanel().getSchoolButton().isInsideButton(mouseX, mouseY)) {

						boolean isButtonPressed = !view.getCalendarHeader().getControlPanel().getSchoolButton().isButtonPressed();

						view.getCalendarHeader().getControlPanel().getSchoolButton().setButtonPressed(
								isButtonPressed);

						highLightEventCards("School");				

					} else if(view.getCalendarHeader().getControlPanel().getOtherButton().isInsideButton(mouseX, mouseY)) {

						boolean isButtonPressed = !view.getCalendarHeader().getControlPanel().getOtherButton().isButtonPressed();

						view.getCalendarHeader().getControlPanel().getOtherButton().setButtonPressed(
								isButtonPressed);

						highLightEventCards("Other");

					} else if(view.getCalendarHeader().getControlPanel().getLowPriorityButton().isInsideButton(mouseX, mouseY)) {

						boolean isButtonPressed = !view.getCalendarHeader().getControlPanel().isLowPriorityClicked();

						view.getCalendarHeader().getControlPanel().getLowPriorityButton().setButtonPressed(isButtonPressed);

						highLightPrioEventCards("Low");

					} else if(view.getCalendarHeader().getControlPanel().getHighPriorityButton().isInsideButton(mouseX, mouseY)) {

						boolean isButtonPressed = !view.getCalendarHeader().getControlPanel().isHighPriorityClicked();

						view.getCalendarHeader().getControlPanel().getHighPriorityButton().setButtonPressed(isButtonPressed);

						highLightPrioEventCards("High");
					}
				}
			}

			if(helpCenterClicked == true) {
				if(glassPane.getCloseButton().isInsideButton(mouseX, mouseY)) {
					glassPane.setVisible(false);
					System.out.println("Remove GlassPane");
					helpCenterClicked = false;
				}
			}

		}

	}

	private void highLightPrioEventCards(String priority) {
		AnimationEngine animationEngine = new AnimationEngine(500);

		if(priority.equalsIgnoreCase("Low")) {			
			view.getCalendarHeader().getControlPanel().setLowPriorityClicked(
					!view.getCalendarHeader().getControlPanel().isLowPriorityClicked());

			if(!view.getCalendarHeader().getControlPanel().isLowPriorityClicked()) {
				animationEngine.setDefaultDirection(AnimationEngine.Direction.BACKWARD);
			}
		} else if(priority.equalsIgnoreCase("High")) {			
			view.getCalendarHeader().getControlPanel().setHighPriorityClicked(
					!view.getCalendarHeader().getControlPanel().isHighPriorityClicked());

			if(!view.getCalendarHeader().getControlPanel().isHighPriorityClicked()) {
				animationEngine.setDefaultDirection(AnimationEngine.Direction.BACKWARD);
			}
		} 

		Iterator<EventCard> iter = view.getEventCardsList().iterator();
		while(iter.hasNext()) {
			EventCard tmp = iter.next();

			if(!tmp.getPriority().equalsIgnoreCase(priority)) {
				animationEngine.addAnimatableTarget(tmp);
			}					
		}
		animationEngine.start();

	}

	private void highLightEventCards(String category) {

		AnimationEngine animationEngine = new AnimationEngine(500);		

		if(category.equalsIgnoreCase("Home")) {			
			view.getCalendarHeader().getControlPanel().setHomeClicked(
					!view.getCalendarHeader().getControlPanel().isHomeClicked());

			if(!view.getCalendarHeader().getControlPanel().isHomeClicked()) {
				animationEngine.setDefaultDirection(AnimationEngine.Direction.BACKWARD);
			}
		} else if (category.equalsIgnoreCase("Work")) {
			view.getCalendarHeader().getControlPanel().setWorkClicked(
					!view.getCalendarHeader().getControlPanel().isWorkClicked());

			//reverse the animation if the work button was clicked again
			if(!view.getCalendarHeader().getControlPanel().isWorkClicked()) {
				animationEngine.setDefaultDirection(AnimationEngine.Direction.BACKWARD);
			}
		} else if (category.equalsIgnoreCase("School")) {
			view.getCalendarHeader().getControlPanel().setSchoolClicked(
					!view.getCalendarHeader().getControlPanel().isSchoolClicked());

			//reverse the animation if the work button was clicked again
			if(!view.getCalendarHeader().getControlPanel().isSchoolClicked()) {
				animationEngine.setDefaultDirection(AnimationEngine.Direction.BACKWARD);
			}
		} else if (category.equalsIgnoreCase("Other")) {
			view.getCalendarHeader().getControlPanel().setOtherClicked(
					!view.getCalendarHeader().getControlPanel().isOtherClicked());

			//reverse the animation if the work button was clicked again
			if(!view.getCalendarHeader().getControlPanel().isOtherClicked()) {
				animationEngine.setDefaultDirection(AnimationEngine.Direction.BACKWARD);
			}
		}

		Iterator<EventCard> iter = view.getEventCardsList().iterator();
		while(iter.hasNext()) {
			EventCard tmp = iter.next();

			if(category.equalsIgnoreCase("Other")) {
				boolean isSchool = tmp.getCategory().equalsIgnoreCase("School");
				boolean isWork = tmp.getCategory().equalsIgnoreCase("Work");
				boolean isHome = tmp.getCategory().equalsIgnoreCase("Home");

				if(isSchool || isWork || isHome) {
					animationEngine.addAnimatableTarget(tmp);
				}
			} else {

				if(!tmp.getCategory().equalsIgnoreCase(category)) {
					animationEngine.addAnimatableTarget(tmp);
				}	
			}			
		}
		animationEngine.start();

	}

	private void fetchNewTasks() {
		Runnable fetchNewEvents = new Runnable() { 
			public void run() {

				ArrayList<EventCard> tempListEventCard = new ArrayList<EventCard>(10);

				Iterator<DayCard> iterDayCard = view.getDayCardList().iterator();
				while(iterDayCard.hasNext()) {
					DayCard tmpDayCard = iterDayCard.next(); 
					Iterator<TaskModel> iter = model.getTasks(CalendarHelper.getYear(), CalendarHelper.getWeekNumber()).iterator();
					while(iter.hasNext()){
						TaskModel tmpTask = iter.next();
						int dayCardDay = Integer.parseInt(tmpDayCard.getDayCardDay());
						if(dayCardDay == tmpTask.getDay()) {
							System.out.println("start-time:"+tmpTask.getStartTime());
							System.out.println("stop-time:"+tmpTask.getStopTime());
							System.out.println("tmpDayCard.getPosX():"+tmpDayCard.getPosX());
							EventCard tmpEventCard = new EventCard(
									tmpDayCard.getPosX(),
									tmpTask.getStartTime(),
									Settings.getDayCardWidth(),
									tmpTask.getStopTime() - tmpTask.getStartTime(),
									tmpTask.getIdNumber());

							tmpEventCard.setStartTime(tmpTask.getStartTime());
							tmpEventCard.setStopTime(tmpTask.getStopTime());
							tmpEventCard.setCategory(tmpTask.getCategory());
							tmpEventCard.setColor(XmlTaskModel.getCategories().getColor(tmpTask.getCategory()));
							tmpEventCard.setContent(tmpTask.getContentTask());
							tmpEventCard.setDay(tmpTask.getDay());
							tmpEventCard.setLocation(tmpTask.getLocation());
							tmpEventCard.setMonth(Integer.parseInt(tmpTask.getMonth()));
							tmpEventCard.setTitle(tmpTask.getTitle());
							tmpEventCard.setWeek(tmpTask.getWeekNum());
							tmpEventCard.setYear(tmpTask.getYear());
							tmpEventCard.setParentDayCardsPosX(tmpDayCard.getPosX());
							tempListEventCard.add(tmpEventCard);
						}
					} 
				}

				view.setEventCardsList(tempListEventCard);
			} 
		};   
		SwingUtilities.invokeLater(fetchNewEvents);
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		System.out.println("mouseReleased: Settings.isAddEventToModel():"+Settings.isAddEventToModel());	
		System.out.println("mouse y: " + event.getY());

		view.mouseReleased(event);

		if(Settings.isAddEventToModel()){
			System.out.println("ADD TASK TO XML");

			Iterator<EventCard> iter = view.getEventCardsList().iterator();
			while(iter.hasNext()) {
				EventCard eventCard = iter.next();
				//System.out.println("ID: eventCard.getId(): "+eventCard.getId() + ", Settings.getEventCardThatToBeAddedToModel():"+Settings.getEventCardThatToBeAddedToModel() +", Settings.getEventCardThatIsChanging()"+Settings.getEventCardThatIsChanging());
				if(eventCard.getId() == Settings.getEventCardThatToBeAddedToModel()) {

					System.out.println("MONTH"+eventCard.getMonth());
					TaskModel task = new TaskModel(
							eventCard.getId(),
							eventCard.getTitle(),
							eventCard.getContent(),
							eventCard.getLocation(),
							eventCard.getCategory(),
							eventCard.getPriority(),
							eventCard.getDay(),
							eventCard.getWeek(),
							""+eventCard.getMonth(),
							eventCard.getYear(),
							eventCard.getStartTime(),
							event.getY());
					model.addTask(task);					
					Settings.setEventCardThatToBeAddedToModel(-1);
					Settings.setAddEventToModel(false);

				}
			}


		} else if(Settings.isPressedOnEventCard()) {
			System.out.println("HAVE BEEN DRAGGED TASK TO XML");
			Iterator<EventCard> iter = view.getEventCardsList().iterator();
			while(iter.hasNext()) {
				EventCard eventCard = iter.next();
				System.out.println("ID: eventCard.getId(): "+eventCard.getId() + ", Settings.getEventCardThatToBeAddedToModel():"+Settings.getEventCardThatToBeAddedToModel() +", Settings.getEventCardThatIsChanging()"+Settings.getEventCardThatIsChanging());
				if(eventCard.getId() == Settings.getEventCardThatIsChanging()) {

					model.updateTask(
							Settings.getEventCardThatIsChanging(),
							eventCard.getTitle(),
							eventCard.getContent(),
							eventCard.getLocation(),
							eventCard.getCategory(),
							eventCard.getPriority(),
							eventCard.getDay(),
							eventCard.getWeek(),
							""+eventCard.getMonth(),
							eventCard.getYear(),
							eventCard.getPosY(),
							eventCard.getHeight()+eventCard.getPosY());
					Settings.setEventCardThatIsChanging(-1);
					Settings.setPressedOnEventCard(false);

				}

			}


		}


		view.repaint();
	}

	private class KeyEvents extends KeyAdapter {

		AnimationEngine aniEngine = new AnimationEngine(5000);
		AnimationEngine aniEngine2 = new AnimationEngine(1000);

		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();

			System.out.println("keycode:"+keyCode);

			if(keyCode == KeyEvent.VK_SHIFT) {
				System.out.println("shift key pressed");
				shiftPressed = true;
			} else if(keyCode == KeyEvent.VK_META) {
				System.out.println("command key pressed");
				commandPressed = true;
			} else if(keyCode == KeyEvent.VK_Z) {
				zPressed = true;
			} else if(keyCode == KeyEvent.VK_CONTROL) {
				ctrlPressed = true;
			} else if (keyCode == KeyEvent.VK_A ) {
				System.out.println("Start animation engine");				
				aniEngine.setDuration(2000);
				aniEngine.setPowerOfN(4);
				aniEngine.setEaseInOut();
				Iterator<EventCard> iter = view.getEventCardsList().iterator();
				while(iter.hasNext()) {
					EventCard tmp = iter.next();
					if(!tmp.getCategory().equalsIgnoreCase("Work")) {
						aniEngine.addAnimatableTarget(tmp);
					} else {
						aniEngine2.addAnimatableTarget(tmp);
					}
				}

				aniEngine.start();
				aniEngine2.start();

			} else if (keyCode == KeyEvent.VK_S) {
				System.out.println("Stopped animation engine");
				aniEngine.stop();
			} else if (keyCode == KeyEvent.VK_P) {
				System.out.println("paused animation engine");
				aniEngine.pause();
			} else if (keyCode == KeyEvent.VK_R) {
				System.out.println("resumed animation engine");
				aniEngine.resume();
			} else if(keyCode == KeyEvent.VK_DELETE) {
				Iterator<EventCard> iter = view.getEventCardsList().iterator();
				while(iter.hasNext()) {
					EventCard tmp = iter.next();
					if(tmp.isClicked()){
						undoRedoController.getUndoRedoModel().pushUndo(tmp);
						iter.remove();
						model.deleteTask(tmp.getId(), tmp.getYear(), tmp.getWeek());
						view.repaint();
					}
				}
			} else if (keyCode == KeyEvent.VK_E) {	
				//view.fighterJet.set
				new Runnable() {					
					@Override
					public void run() {
						view.fighterJet.start();

					}
				}.run();

			} else if (keyCode == KeyEvent.VK_M) {				
				bezier = !bezier;
				p.print("ADD BEZIER POINT:"+bezier);

				if(!bezier) {
					p.print("GENERATING BEZIER PATH");
					bpath.generatePath();

					view.fighterJet.setPath(bpath);
				} else {
					view.dots.clear();
					bpath.clear();
					view.repaint();
					
				}

			}



		}

		public void keyReleased(KeyEvent e) {			

			int keyCode = e.getKeyCode();
			if((commandPressed || ctrlPressed) && zPressed) {				
				undo();
			} else if(shiftPressed && zPressed) {
				redo();
			}

			if(keyCode == KeyEvent.VK_SHIFT) {
				System.out.println("shift key released");
				shiftPressed = false;

			} else if(keyCode == KeyEvent.VK_META) {
				System.out.println("command key pressed");
				commandPressed = false;
			} else if(keyCode == KeyEvent.VK_Z) {
				zPressed = false;
			} else if(keyCode == KeyEvent.VK_CONTROL) {
				ctrlPressed = false;
			}
		}



	}

	private class MenuEventHandler implements ActionListener {

		public MenuEventHandler() {			
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			Object source = event.getSource();
			if(menu.getMenuZeroPercent() == source){
				System.out.println("getMenuZeroPercent");
				if(AWTUtilities.isWindowOpaque(mainFrame)) {
					AWTUtilities.setWindowOpacity(mainFrame, 1.0f);
				}
			} else if(menu.getMenuTwentyfivePercent() == source) {
				System.out.println("getMenuTwentyfivePercent");
				if(AWTUtilities.isWindowOpaque(mainFrame)) {
					AWTUtilities.setWindowOpacity(mainFrame, 0.75f);
				}
			} else if(menu.getMenuFiftyPercent() == source) {
				System.out.println("getMenuFiftyPercent");
				if(AWTUtilities.isWindowOpaque(mainFrame)) {
					AWTUtilities.setWindowOpacity(mainFrame, 0.50f);
				}
			} else if(menu.getMenuSeventyfivePercent() == source) {
				System.out.println("getMenuSeventyfivePercent");
				if(AWTUtilities.isWindowOpaque(mainFrame)) {
					AWTUtilities.setWindowOpacity(mainFrame, 0.25f);
				}
			} else if(menu.getMenuQuit() == source) {
				mainFrame.dispose();
			} else if(menu.getMenuUndo() == source) {
				undo();
			} else if(menu.getMenuRedo() == source) {
				redo();
			} else if(menu.getMenuDelete() == source) {
				Iterator<EventCard> iter = view.getEventCardsList().iterator();
				while(iter.hasNext()) {
					EventCard tmp = iter.next();
					if(tmp.isClicked()){
						undoRedoController.getUndoRedoModel().pushUndo(tmp);
						iter.remove();
						model.deleteTask(tmp.getId(), tmp.getYear(), tmp.getWeek());
						view.repaint();
					}
				}
			} else if(menu.getMenuAbout() == source) {
				new JOptionPane();
				JOptionPane.showMessageDialog(mainFrame,
						"This calendar is created by\n		Jonathan Andersson\n		Erik Hellberg\n		Henrik Forsberg\n		Erik �berg",
						"About this calendar",
						JOptionPane.INFORMATION_MESSAGE);
			} else if(menu.getMenuHelpCenter() == source) {
				glassPane.setVisible(true);
				System.out.println("Draw GlassPane");
				helpCenterClicked = true;
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String title = popup.getTitleField();
		String content = popup.getContentField();
		String location = popup.getLocationField();
		String category = popup.getCategoryField();
		System.out.println("CATEGORY SAVE BUTTON:"+category);
		String priority = popup.getPriorityField();
		Iterator<EventCard> iter = view.getEventCardsList().iterator();
		while(iter.hasNext()) {
			EventCard tmp = iter.next();
			if(tmp.getId() == recentTaskId) {
				tmp.setTitle(title);
				tmp.setContent(content);
				tmp.setLocation(location);
				tmp.setCategory(category);
				tmp.setColor(XmlTaskModel.getCategories().getColor(category));
				tmp.setPriority(priority);
				tmp.setDay(recentTaskDay);
				tmp.setWeek(recentTaskWeek);
				tmp.setMonth(Integer.parseInt(recentTaskMonth));
				tmp.setYear(recentTaskYear);
				tmp.setStartTime(recentTaskStartTime);
				tmp.setStopTime(recentTaskStopTime);
			}
		}
		model.updateTask(recentTaskId, title, content, location, category, priority, 
				recentTaskDay, recentTaskWeek, recentTaskMonth, recentTaskYear, recentTaskStartTime, recentTaskStopTime);
		popup.dispose();
		popupIsOpen = false;
		view.repaint();
	}

	private void undo() {
		tmpEventCardToUndo = undoRedoController.getUndoRedoModel().undo();
		if(tmpEventCardToUndo != null) {
			System.out.println("Undo: cmd and z i pressed");
			//System.out.println("posX:"+tmpEventCardToUndo.getPosX());
			System.out.println("Undo list size:" + undoRedoController.getUndoRedoModel().getUndoStack().getList().size());

			System.out.println("SIZE:"+view.getEventCardsList().size());
			Runnable undoEvent = new Runnable() { 
				public void run() {

					Iterator<EventCard> iter = view.getEventCardsList().iterator();

					while(iter.hasNext()) {
						EventCard tmpEventCard = iter.next();
						if(tmpEventCardToUndo.getId() == tmpEventCard.getId()) {									
							//may need a thread 
							undoRedoController.getUndoRedoModel().pushRedo(tmpEventCard);
							model.deleteTask(tmpEventCard.getId(), tmpEventCard.getYear(), tmpEventCard.getWeek());
							iter.remove();

						}

					}
					System.out.println("SIZE:"+view.getEventCardsList().size());

					System.out.println("posX:"+tmpEventCardToUndo.getPosX());
					System.out.println("UNDO:"+tmpEventCardToUndo.getId());

					TaskModel tmpTask  = new TaskModel(
							tmpEventCardToUndo.getId(), 
							tmpEventCardToUndo.getTitle(),
							tmpEventCardToUndo.getContent(),
							tmpEventCardToUndo.getLocation(),
							tmpEventCardToUndo.getCategory(),
							tmpEventCardToUndo.getPriority(),
							tmpEventCardToUndo.getDay(),
							tmpEventCardToUndo.getWeek(),
							""+tmpEventCardToUndo.getMonth(),
							tmpEventCardToUndo.getYear(),
							tmpEventCardToUndo.getStartTime(),
							tmpEventCardToUndo.getStopTime());
					model.addTask(tmpTask);
					view.getEventCardsList().add(tmpEventCardToUndo);
					System.out.println("SIZE:"+view.getEventCardsList().size());
					view.repaint();
				} 
			};   
			SwingUtilities.invokeLater(undoEvent);					
		}

		zPressed = false;

	}


	private void redo() {
		tmpEventCardToRedo = undoRedoController.getUndoRedoModel().redo();
		if(tmpEventCardToRedo != null) {



			Runnable redoEvent = new Runnable() { 
				public void run() {

					Iterator<EventCard> iter = view.getEventCardsList().iterator();

					while(iter.hasNext()) {
						EventCard tmpEventCard = iter.next();
						if(tmpEventCardToRedo.getId() == tmpEventCard.getId()) {									
							//may need a thread 
							undoRedoController.getUndoRedoModel().pushUndo(tmpEventCard);
							model.deleteTask(tmpEventCard.getId(), tmpEventCard.getYear(), tmpEventCard.getWeek());
							iter.remove();

						}

					}
					System.out.println("SIZE:"+view.getEventCardsList().size());

					System.out.println("posX:"+tmpEventCardToRedo.getPosX());
					System.out.println("UNDO:"+tmpEventCardToRedo.getId());

					TaskModel tmpTask  = new TaskModel(
							tmpEventCardToRedo.getId(), 
							tmpEventCardToRedo.getTitle(),
							tmpEventCardToRedo.getContent(),
							tmpEventCardToRedo.getLocation(),
							tmpEventCardToRedo.getCategory(),
							tmpEventCardToRedo.getPriority(),
							tmpEventCardToRedo.getDay(),
							tmpEventCardToRedo.getWeek(),
							""+tmpEventCardToRedo.getMonth(),
							tmpEventCardToRedo.getYear(),
							tmpEventCardToRedo.getStartTime(),
							tmpEventCardToRedo.getStopTime());
					model.addTask(tmpTask);
					view.getEventCardsList().add(tmpEventCardToRedo);
					System.out.println("SIZE:"+view.getEventCardsList().size());
					view.repaint();
				} 
			};   
			SwingUtilities.invokeLater(redoEvent);	

		}
		zPressed = false;

	}


}
