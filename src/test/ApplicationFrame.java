package test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.Timer;

public class ApplicationFrame extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6690903557951897614L;



	public ApplicationFrame() { 
		this("ApplicationFrame v1.1"); 
	}

	public ApplicationFrame(String title) {
		super(title);		
		createUI();
	}

	protected void createUI() {
		setPreferredSize(new Dimension(500, 500));
		center();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
	}


	
	public void center() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = getSize();
		int x = (screenSize.width - frameSize.width) / 2;
		int y = (screenSize.height - frameSize.height) / 2;
		setLocation(x, y);
	}




	public static void main(String[] args) {
		ApplicationFrame frame = new ApplicationFrame();

		CustomJPanel panel = new CustomJPanel();
		panel.setBackground(Color.GRAY);
		frame.add(panel);

		frame.pack();
		frame.setVisible(true);
	}

	








}
