package test;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class CustomJPanel extends JPanel implements ActionListener, MouseMotionListener,MouseListener {

	
	private static final boolean DRAG_AND_DROP = true;
	private static final boolean RESIZE = true; 
	
	
	TestRectangle rect;
	Timer timer;
	private boolean wasInside = false;
	private boolean resizeRight = false;
	private boolean resizeBottom = false;
	private boolean resizeLeft = false;
	private boolean resizeUpper = false;
	
	public CustomJPanel() {
		rect = new TestRectangle(10, 10, 100, 100);
		setPreferredSize(new Dimension(200,200));
		System.out.println("CustomJPanel");
		addMouseListener(this);
		addMouseMotionListener(this);
		//timer = new Timer(500,this);
		//timer.start();
	}
	
	
	
	
	
	@Override
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);		
		rect.draw(g);
	}


	@Override
	public void actionPerformed(ActionEvent e) {		
		this.repaint();		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		
		int recX = rect.x;
		int recY = rect.y;		
		
		
		int recWidth = rect.width;
		int recHeight = rect.height;
		
		if(mouseX >= recX &&  mouseX <= (recWidth+recX)  && mouseY >= recY && mouseY <= (recHeight+recY)) {
			System.out.println("inside rectangle");	
			wasInside = true;
		}
		
		if(RESIZE) {
			if(onRightBorder(mouseX) && onBottomBorder(mouseY)) {
				System.out.println("right border");
				System.out.println("bottom border");
				resizeRight = true;
				resizeBottom = true;
			} else if(onRightBorder(mouseX)) {
				System.out.println("right border");
				resizeRight = true;
			}
		}
		System.out.println("mouse x:"+mouseX);
		System.out.println("right border coord:"+rect.rightBorderCord);
		
		
	}

	
	@Override
	public void mouseDragged(MouseEvent event) {
		//System.out.println("mouseDragged");
		if(DRAG_AND_DROP) {
			if(wasInside) {
				int rectCenterX = rect.width / 2;
				int rectCenterY = rect.height / 2;
				rect.x = event.getX() - rectCenterX;
				rect.y = event.getY() - rectCenterY;
				repaint();			
			}
		} 
		if(RESIZE) {
			System.out.println("resizeRight:"+resizeRight + "  resizeBottom:"+resizeBottom);
			
			if(resizeRight && resizeBottom) {
				if(event.getX() > rect.width && event.getY() > rect.height) {
					rect.width += (event.getX() - rect.width);	
					rect.rightBorderCord = rect.x +rect.width;
				
					rect.height += (event.getY() - rect.height);
				    rect.bottomBorderCord = rect.y + rect.height ;
				} else {					
					rect.width = event.getX();
					rect.height = event.getY();
					rect.rightBorderCord = rect.x + rect.width;
					rect.bottomBorderCord = rect.y +rect.height;
				}
				
			} else if(resizeRight) {
				if(event.getX() > rect.width ) {
					rect.width += (event.getX() - rect.width);
					rect.rightBorderCord = rect.x +rect.width;
				} else {					
					rect.width = event.getX();
					rect.rightBorderCord = rect.x + rect.width;
				}
			}	
			repaint();
		}		
	}


	@Override
	public void mouseMoved(MouseEvent event) {		
		if(onRightBorder(event.getX())) {
			System.out.println("entered right border");
		}
		if(onLeftBorder(event.getX())) {
			System.out.println("entered left border");
		}
		if(onUpperBorder(event.getY())) {
			System.out.println("entered upper border");
		}
		if(onBottomBorder(event.getY())) {
			System.out.println("entered bottom border");
		}
		
	}


	@Override
	public void mouseClicked(MouseEvent e) {
		
	}


	@Override
	public void mouseEntered(MouseEvent event) {
		
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("mouseReleased");
		wasInside = false;
		resizeRight = false;
		resizeBottom = false;
		repaint();
	}
	
	
	public boolean onRightBorder(int mouseX){
		if(rect.rightBorderCord == mouseX || rect.rightBorderCord == mouseX-1 || rect.rightBorderCord == mouseX+1){
			
			this.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
			return true;
		}
		this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		return false;
	}
	
	public boolean onLeftBorder(int mouseX){
		if(rect.leftBorderCord == mouseX || rect.leftBorderCord == mouseX-1 || rect.leftBorderCord == mouseX+1){
			this.setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
			return true;
		}
		return false;
	}
	
	public boolean onUpperBorder(int mouseY){
		if(rect.upperBorderCord == mouseY || rect.upperBorderCord == mouseY-1 || rect.upperBorderCord == mouseY+1){
			this.setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
			return true;
		}
		return false;
	}
	
	
	public boolean onBottomBorder(int mouseY){
		if(rect.bottomBorderCord == mouseY || rect.bottomBorderCord == mouseY-1 || rect.bottomBorderCord == mouseY+1){
			this.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
			return true;
		}
		return false;
	}
}
