package test;

import interfaces.Drawable;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;


public class TestRectangle implements Drawable{

	int x;
	int y;
	int width;
	int height;
	int leftBorderCord;
	int rightBorderCord;
	int upperBorderCord;
	int bottomBorderCord;
	
	public TestRectangle(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.leftBorderCord = x;
		this.rightBorderCord = width+x;
		this.upperBorderCord = y;
		this.bottomBorderCord = height+y;
	}


	@Override
	public void draw(Graphics g) {		
		Graphics2D gTemp = (Graphics2D)g.create();
		gTemp.setColor(Color.black);
	//	gTemp.fillRect(x, y, width, height);		
		RenderingHints renderHints =
				  new RenderingHints(RenderingHints.KEY_ANTIALIASING,
				                     RenderingHints.VALUE_ANTIALIAS_ON);
				renderHints.put(RenderingHints.KEY_RENDERING,
				                RenderingHints.VALUE_RENDER_QUALITY);
		gTemp.setRenderingHints(renderHints);
		gTemp.fillRoundRect(x, y, width, height, 10, 10);
		gTemp.dispose();
		
	}

	
	

}
