package test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JComponent;

//First technique: JComponent
public class GlassPaneTest extends JComponent {
	@Override
	protected void paintComponent(Graphics g) {
		Rectangle clip = g.getClipBounds();
		Color alphaWhite = new Color(1.0f, 1.0f, 1.0f, 0.65f);
		g.setColor(alphaWhite);
		g.fillRect(clip.x, clip.y, clip.width, clip.height);
	}
}