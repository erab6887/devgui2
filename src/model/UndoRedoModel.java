package model;

import view.components.EventCard;
import interfaces.UndoRedoInterface;

public class UndoRedoModel implements UndoRedoInterface{

	private ECStack undoStack;
	private ECStack redoStack;
	
	public UndoRedoModel() {
		undoStack = new ECStack();
		redoStack = new ECStack();
	}

	@Override
	public EventCard undo() {
		EventCard tmpEventCard = undoStack.pop();
		if(tmpEventCard != null) {			
			System.out.println("UNDO-UNDO:"+tmpEventCard.getDay());			
		}
		return tmpEventCard;
		
	}

	@Override
	public EventCard redo() {
		EventCard tmpEventCard = redoStack.pop();
		if(tmpEventCard != null) {
			
			System.out.println("REDO-REDO:"+tmpEventCard.getDay());
			
		}
		return tmpEventCard;
		
	}
	
	
	public void pushUndo(EventCard eventCard) {
		undoStack.push(eventCard);
	}
	
	public void pushRedo(EventCard eventCard) {
		redoStack.push(eventCard);
	}

	public ECStack getUndoStack() {
		return undoStack;
	}
	

	public ECStack getRedoStack() {
		return redoStack;
	}

	
	
	
	
}
