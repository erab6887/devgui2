package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.jdom.Attribute;
import org.jdom.Element;


/**
 * 
 * A task will look like below. It's extends the JDOM Element. 
 * 
 * 	<?xml version="1.0" ?>
 *	<!DOXTYPE tasks SYSTEM "tasks.dtd"> 
 *	<tasks>
 *		<task id="1" createdOn="2012-01-01" updatedOn="2012-01-02">
 *			<title>Some title</title>
 *			<content>This is the content</content>
 *  		<location>Some location</location>
 * 			<category>School</category>
 * 			<day>13</day>
 * 			<week>16</week>
 * 			<month>April</month>
 * 			<year>2012</year>
 *  		<start-time>1800</start-time>
 * 			<stop-time>1830</stop-time>
 *		</task>
 *	</tasks>
 *
 * @author
 *
 */
public class TaskModel extends Element {


	private static final String PATH_TO_ID_FILE = "data/id.txt";

	
	
	private static File idFile;
	private String title;
	private String content;
	private String location;
	private String category;
	private String priority;
	private int idNumber;
	private int day;
	private int week;	
	private String month;
	private int year;	
	private int startTime;
	private int stopTime;
	private static final long serialVersionUID = 5906146024846122344L;

	/**
	 * 
	 * @param title. the title of the task
	 * @param content, the content of the task
	 * @param location, location where the task will occur
	 * @param category, category of the task
	 * @param day, the day the task belongs to
	 * @param week, the week the task belongs to
	 * @param month, the month the task belongs to.
	 * @param year, the year the task belongs to
	 * @param startTime, when the task begins
	 * @param stopTime, when the task ends	
	 */
	public TaskModel(int id, String title,String content,String location,String category,String priority,
			int day,int week,String month, int year,int startTime, int stopTime) {
		super("task");
		idFile = new File(PATH_TO_ID_FILE);	
		
		this.title = title;
		this.content = content;
		this.location = location;
		this.category = category;
		this.priority = priority;
		this.day = day;
		this.week = week;
		this.month = month;
		this.year = year;
		this.startTime = startTime;
		this.stopTime = stopTime;
		String idString = ""+id;//call to makeId
		this.idNumber = id;
		
		
		Attribute attrId = new Attribute("id",idString); 		
		this.setAttribute(attrId);			
		this.addContent(new Element("title").setText(title));
		this.addContent(new Element("content").setText(content));
		this.addContent(new Element("location").setText(location));
		this.addContent(new Element("category").setText(category));
		this.addContent(new Element("priority").setText(priority));
		this.addContent(new Element("day").setText(""+day));
		this.addContent(new Element("week").setText(""+week));
		this.addContent(new Element("month").setText(month));
		this.addContent(new Element("year").setText(""+year));
		this.addContent(new Element("start-time").setText(""+startTime));
		this.addContent(new Element("stop-time").setText(""+stopTime));

	}

	
	/**
	 * TODO make use of reusable id numbers 
	 * 
	 * makeId makes a unique id that should be added to a Task
	 * 
	 * @return a unique id 
	 */
	public static synchronized String makeId(){
		String id = "1";
		PrintWriter outputStream = null;
		idFile = new File(PATH_TO_ID_FILE);	
		if(idFile.isFile()){
			Scanner inputStream = null;
			try {
				int tmpId = 0;
				inputStream = new Scanner(new FileInputStream(getPathToIdFile()));
				while(inputStream.hasNextInt()){
					tmpId = inputStream.nextInt();
				}
				tmpId++;				
				id = ""+tmpId;				
				outputStream = new PrintWriter(new FileOutputStream(getPathToIdFile()));
				outputStream.print(tmpId);
				outputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}else {
			try {
				outputStream = new PrintWriter(new FileOutputStream(getPathToIdFile()));
				outputStream.print(id);
				outputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return id;
	}
	
	
	
	
	
	public int getDay() {
		return day;
	}


	public void setDay(int day) {
		this.day = day;
	}


	public String getMonth() {
		return month;
	}


	public void setMonth(String month) {
		this.month = month;
	}


	public int getStartTime() {
		return startTime;
	}


	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}


	public int getStopTime() {
		return stopTime;
	}


	public void setStopTime(int stopTime) {
		this.stopTime = stopTime;
	}


	public int getWeekNum() {
		return week;
	}


	public void setWeekNum(int weekNum) {
		this.week = weekNum;
	}

	
	

	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContentTask() {
		return this.content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getLocation() {
		return this.location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getCategory() {
		return this.category;
	}


	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getPriority() {
		return this.priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}

	
	public int getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}


	

	/**
	 * 
	 * @return An abstract representation of file that holds the max id for all tasks 
	 */
	private static File getIdFile(){
		return TaskModel.idFile;
	}		

	
	
	private static String getPathToIdFile(){
		return TaskModel.PATH_TO_ID_FILE;
	}


	
	public String toString(){
		return this.title;
	}
}
