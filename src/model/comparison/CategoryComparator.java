package model.comparison;

import java.util.Comparator;

import model.TaskModel;

/**
 * 
 * Comparator for categories.
 *
 */
public class CategoryComparator implements Comparator<TaskModel> {

	@Override
	public int compare(TaskModel task1, TaskModel task2) {
		// TODO Auto-generated method stub
		int res = task1.getCategory().compareTo(task2.getCategory());
		if(res == 0) {
			return res;
		} else {
			return res*-1; //negate the result to reverse the sort order
		}
	}

}
