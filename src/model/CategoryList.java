package model;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * <categories>
 * 		<category category="Work">
 *			<red></red>
 *			<green></green>
 *			<blue></blue>
 *			<alpha></alpha>
 *		</category>
 *		...
 *		...
 * </categories>
 *
 */

public class CategoryList {



	private static final String PATH_TO_XML_FILE = "data/categories.xml";

	private Document doc;

	private ArrayList<CategoryModel> list; 
	private HashMap<String,CategoryModel> map; 

	public CategoryList() {			
		list = new ArrayList<CategoryModel>(10);
		map = new HashMap<String, CategoryModel>();
		SAXBuilder builder = new SAXBuilder();



		try {
			File file = new File(PATH_TO_XML_FILE);
			if(!file.isFile()){
				FileWriter fstream = new FileWriter(PATH_TO_XML_FILE);
				BufferedWriter out = new BufferedWriter(fstream);
				out.write("");
				out.close();
				doc = new Document(new Element("categories"));

				CategoryModel work = new CategoryModel("Work", 200, 200, 255);
				CategoryModel home = new CategoryModel("Home", 200, 255, 200);
				CategoryModel school = new CategoryModel("School", 255, 200, 200);
				CategoryModel other = new CategoryModel("Other", 255,255,150);
				addCategory(work);
				addCategory(home);
				addCategory(school);
				addCategory(other);


				storeChangesToFile();

				
				doc = builder.build(PATH_TO_XML_FILE);


			} else {

				doc = builder.build(PATH_TO_XML_FILE);			


				if(doc.hasRootElement()){				

					@SuppressWarnings("unchecked")
					List<Element> jdomCategories  = doc.getRootElement().getChildren();
					Iterator<Element> iter = jdomCategories.iterator();
					while(iter.hasNext()) {
						Element tmp = iter.next();

						String type = tmp.getAttributeValue("category");
						int red = Integer.parseInt(tmp.getChildText("red"));
						int green = Integer.parseInt(tmp.getChildText("green"));
						int blue = Integer.parseInt(tmp.getChildText("blue"));
						int alpha = Integer.parseInt(tmp.getChildText("alpha"));

						CategoryModel category = new CategoryModel(type, red, green, blue, alpha);

						list.add(category);
						map.put(type, category);

					}

				}
			}


		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void addCategory(CategoryModel category) {


		list.add(category);	
		map.put(category.getCategory(), category);
		Iterator<CategoryModel> iter = list.iterator();
		while(iter.hasNext()) {
			CategoryModel tmp = iter.next();
			System.out.println("category.getCategory() = "+category.getCategory());
			System.out.println("tmp.getCategory() = "+tmp.getCategory());
			if(category.getCategory().equalsIgnoreCase(tmp.getCategory())) {
				tmp.setRed(category.getRed());
				tmp.setGreen(category.getGreen());
				tmp.setBlue(category.getBlue());
				tmp.setAlpha(category.getAlpha());

				Element root = doc.getRootElement();
				map.remove(category.getCategory());

				@SuppressWarnings("unchecked")
				List<Element> allCategories = root.getChildren();
				Iterator<Element> iterJdom = allCategories.iterator();
				while(iterJdom.hasNext()) {
					Element tmpCategory = iterJdom.next();
					String type = tmpCategory.getAttributeValue("category");
					if(type.equalsIgnoreCase(category.getCategory())){
						iterJdom.remove();							
					}
				}
			}
		}	

		doc.getRootElement().addContent(category);
		//printDoc();
		storeChangesToFile();
	}



	public ArrayList<CategoryModel> getList() {
		return list;
	}

	public CategoryModel getCategoryModel(String category) {
		
		Iterator<CategoryModel> iter = list.iterator();
		while(iter.hasNext()) {
			CategoryModel tmp = iter.next(); 
			if(tmp.getCategory().equalsIgnoreCase(category)){
				return tmp;
			}			
		}
		return null; 
	}
	
	public Color getColor(String category){

		Iterator<CategoryModel> iter = list.iterator();
		while(iter.hasNext()) {
			CategoryModel tmp = iter.next(); 
			if(tmp.getCategory().equalsIgnoreCase(category)){
				return tmp.getColor();
			}			
		}
		return null;
	}

	public void storeChangesToFile(){
		try{

			XMLOutputter outp = new XMLOutputter();			
			Format format = Format.getPrettyFormat();
			outp.setFormat(format);

			// Create file 
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PATH_TO_XML_FILE),"UTF8"));

			outp.output(doc, out);	

			out.close();


		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public void printDoc(){
		XMLOutputter outp = new XMLOutputter();
		Format format = Format.getPrettyFormat();
		outp.setFormat(format);
		try {
			outp.output(doc, System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
