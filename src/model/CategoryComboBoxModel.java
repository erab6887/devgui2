package model;

import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;



public class CategoryComboBoxModel extends AbstractListModel implements MutableComboBoxModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4083355461702396132L;



	private ArrayList<CategoryModel> categories;;

	private CategoryModel selectedCategory;

	public CategoryComboBoxModel(ArrayList<CategoryModel> categories){
		this.categories = categories;


	}


	@Override
	public CategoryModel getElementAt(int index) {
		return categories.get(index);		
	}

	@Override
	public int getSize() {
		return categories.size();
	}


	@Override
	public CategoryModel getSelectedItem() {
		return selectedCategory;
	}


	@Override
	public void setSelectedItem(Object newItem) {
		this.selectedCategory = (CategoryModel)newItem;

	}


	@Override
	public void addElement(Object obj) {

		categories.add((CategoryModel)obj);
		System.out.println("BIG DICK!");
		
		int length = getSize();
		fireIntervalAdded(this, length-1, length-1);

	}


	@Override
	public void insertElementAt(Object obj, int index) {
		categories.add(index, (CategoryModel)obj);

		fireIntervalAdded(this, index, index);

	}


	@Override
	public void removeElement(Object obj) {
		int index = categories.indexOf((CategoryModel)obj);


		if (index != -1) {

			categories.remove((CategoryModel)obj);

			fireIntervalRemoved(this, index, index);
		}

	}


	@Override
	public void removeElementAt(int index) {
		if (getSize() >= index) {

			categories.remove(index);

			fireIntervalRemoved(this, index, index);
		}

	}

	/*
	public void add(CategoryModel category) {
		if (categories.add(category)) {
			fireContentsChanged(this, 0, getSize());
		}
	}


	public void addAll(ArrayList<CategoryModel> categories) {
		Collection<CategoryModel> c = categories;
		this.categories.addAll(c);
		fireContentsChanged(this, 0, getSize());
	}


	public void clear() {		
		categories.clear();
		fireContentsChanged(this, 0, getSize());
	}

	public boolean contains(TaskModel task) {
		return categories.contains(task);
	}

	public CategoryModel firstElement() {
	    return categories.get(0);
	}


	public Iterator<CategoryModel> iterator() {
	    return categories.iterator();
	}

	public CategoryModel lastElement() {
	    return categories.get(getSize()-1);
	}

	public boolean removeElement(CategoryModel category) {	
	    boolean removed = categories.remove(category);
	    if (removed) {
	      fireContentsChanged(this, 0, getSize());
	    }
	    return removed;
	  }*/
}
