package model;

/**<?xml version="1.0" ?>
*	<!DOXTYPE tasks SYSTEM "tasks.dtd"> 
*	
*		<category category="Work">
*			<red></red>
*			<green></green>
*			<blue></blue>
*			<alpha></alpha>
*		</category>
*	
**/

import java.awt.Color;

import org.jdom.Attribute;
import org.jdom.Element;

public class CategoryModel extends Element {

	private String category;
	private int red;
	private int green;
	private int blue;
	private int alpha = 230;
	
	
	public CategoryModel(String category,int red,int green, int blue) {
		this(category,red,green,blue,230);
	}
	
	
	public CategoryModel(String category,int red, int green,int blue, int alpha) {
		super("category");
		
		this.category = category;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;		
		
		Attribute attrId = new Attribute("category",category); 		
		this.setAttribute(attrId);			
		this.addContent(new Element("red").setText(""+red));
		this.addContent(new Element("green").setText(""+green));
		this.addContent(new Element("blue").setText(""+blue));
		this.addContent(new Element("alpha").setText(""+alpha));
		
		
		
	}
	//@Override
	public boolean equals(CategoryModel obj){
		return this.category.equalsIgnoreCase(obj.category);
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public int getAlpha() {
		return alpha;
	}

	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}

	public Color getColor() {
		return new Color(red,green,blue,alpha);
	}	
	
	public String toString() {
		return this.category;
	}
	
	
}
