package model;

import java.util.ArrayList;
import java.util.Iterator;

import view.components.EventCard;

public class ECStack {

	private ArrayList<EventCard> list;

	public ECStack() {
		this.list = new ArrayList<EventCard>(50);

	}

	public EventCard pop() {
		int listLength = list.size();
		if(listLength > 0) {
			EventCard tmpEventCard = list.remove(listLength-1);
			return tmpEventCard;		
		} else
			return null;
	}
	
	public void push(EventCard eventCard) {
		list.add(eventCard);
	}
	
	public Iterator<EventCard> getIterator() {
		return list.iterator();
	}
	
	public ArrayList<EventCard> getList() {
		return this.list;
	}
	
}
