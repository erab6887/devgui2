package model;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * Class that handles operations on the different XML-files that is used has storing facility 
 * for the T0DO-Manager application * 
 * 
 * @author 
 *
 */
public class XmlTaskModel {

	
	private static final String PATH_TO_ID_FILE = "data/id.txt";

	private static File idFile = new File(PATH_TO_ID_FILE);
	
	

	protected static final String PATH_TO_XML_FILE = "data/tasks.xml";

	protected Document doc;

	private static CategoryList categories;

	//the first hash map will hash on year, and return a hash map which will
	//hash on week number and returns an ArrayList with tasks within that week
	private HashMap<Integer,HashMap<Integer,ArrayList<TaskModel>>> tasks = null;


	@SuppressWarnings("unchecked")
	public XmlTaskModel(){

		categories = new CategoryList();
				
		System.out.println("###########Categories size: "+categories.getList().size());
		
		
		SAXBuilder builder = new SAXBuilder();
		tasks  = new HashMap<Integer,HashMap<Integer,ArrayList<TaskModel>>> ();
		
		
		
		try {
			File file = new File(PATH_TO_XML_FILE);
			// if task.xml is missing create it
			if(!file.isFile()){
				FileWriter fstream = new FileWriter(PATH_TO_XML_FILE);
				BufferedWriter out = new BufferedWriter(fstream);
				out.write("");
				out.close();
				doc = new Document(new Element("tasks"));
				storeChangesToFile();

			}			
			doc = builder.build(PATH_TO_XML_FILE);

			if(getDoc().hasRootElement()){
				List<Element> jdomTasks  = getDoc().getRootElement().getChildren();
				Iterator<Element> iter = jdomTasks.iterator();
				while(iter.hasNext()){

					Element tmp = iter.next();
					String id = tmp.getAttributeValue("id");

					int day = Integer.parseInt(tmp.getChildText("day"));
					int week = Integer.parseInt(tmp.getChildText("week"));

					int year = Integer.parseInt(tmp.getChildText("year"));
					int startTime = Integer.parseInt(tmp.getChildText("start-time"));
					int stopTime = Integer.parseInt(tmp.getChildText("stop-time"));

					TaskModel tmpTask = new TaskModel(Integer.parseInt(id),tmp.getChildText("title"),tmp.getChildText("content"),tmp.getChildText("location")
							,tmp.getChildText("category"),tmp.getChildText("priority"),day,week,tmp.getChildText("month"),year,startTime,stopTime);

					tmpTask.setIdNumber(Integer.parseInt(id));

					if(tasks.containsKey(year)) { // if inner hash map exists
						if(tasks.get(year).containsKey(week)) {
							tasks.get(year).get(week).add(tmpTask);
						} else {
							tasks.get(year).put(week, new ArrayList<TaskModel>(10));
							tasks.get(year).get(week).add(tmpTask);
						}

					} else {

						tasks.put(year, new HashMap<Integer,ArrayList<TaskModel>>());							
						tasks.get(year).put(week, new ArrayList<TaskModel>(10));
						tasks.get(year).get(week).add(tmpTask);		

					} 
				}
			}
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * Add a new task to the document given by the getDoc() method.
	 * Also store the task to the path given by the getPathToXmlFile() method 
	 * 
	 * @param task, the new task to be added
	 */
	public void addTask(TaskModel task){

		if(tasks.containsKey(task.getYear())) { // if inner hash map exists
			if(tasks.get(task.getYear()).containsKey(task.getWeekNum())) {
				tasks.get(task.getYear()).get(task.getWeekNum()).add(task);
			} else {
				tasks.get(task.getYear()).put(task.getWeekNum(), new ArrayList<TaskModel>(10));
				tasks.get(task.getYear()).get(task.getWeekNum()).add(task);
			}

		} else {

			tasks.put(task.getYear(), new HashMap<Integer,ArrayList<TaskModel>>());							
			tasks.get(task.getYear()).put(task.getWeekNum(), new ArrayList<TaskModel>(10));
			tasks.get(task.getYear()).get(task.getWeekNum()).add(task);		

		} 

		getDoc().getRootElement().addContent(task);
		storeChangesToFile();
		
	}


	public void printAllTasks(ArrayList<TaskModel> list ){
		Iterator<TaskModel> iter = list.iterator();
		System.out.println("-------------------------");
		while(iter.hasNext()){
			TaskModel task = iter.next();
			System.out.println("ID:"+task.getIdNumber());
			System.out.println("Title:"+task.getTitle());
			System.out.println("Content:"+task.getContentTask());
			System.out.println("Location:"+task.getLocation());	
			System.out.println("Category:"+task.getCategory());
			System.out.println("Priority:"+task.getPriority());
			System.out.println("-------------------------");
		}

	}

	public void printAllTasks(HashMap<Integer,TaskModel> map ){
		Iterator<Entry<Integer,TaskModel>> iter = map.entrySet().iterator();
		System.out.println("-------------------------");
		while(iter.hasNext()){
			Entry<Integer,TaskModel> entry = iter.next();
			System.out.println("Key:"+entry.getKey().toString());
			System.out.println("ID:"+entry.getValue().getIdNumber());
			System.out.println("Title:"+entry.getValue().getTitle());
			System.out.println("Content:"+entry.getValue().getContentTask());
			System.out.println("Location:"+entry.getValue().getLocation());	
			System.out.println("Category:"+entry.getValue().getCategory());
			System.out.println("Priority:"+entry.getValue().getPriority());
			System.out.println("-------------------------");
		}

	}


	/**
	 * 
	 * 
	 * @return the JDOM doc
	 */
	public  Document getDoc(){
		return this.doc;
	}

	public  String getPathToXmlFile(){
		return PATH_TO_XML_FILE;
	}

	/**
	 * 
	 * @param id, id of the task to be updated
	 * @param title, the new title of the task
	 * @param content, the new content of the task
	 * @param location, the new location of the task
	 * @param category, the new category of the task
	 * @param day, the day the task belongs to
	 * @param week, the week the task belongs to
	 * @param month, the month the task belongs to.
	 * @param year, the year the task belongs to
	 * @param startTime, when the task begins
	 * @param stopTime, when the task ends
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean updateTask(int id,String title,String content,String location,String category,String priority,
			int day,int week,String month, int year,int startTime, int stopTime){

		boolean result = false;
		Element root = getDoc().getRootElement();

		List<Element> allTasks = root.getChildren();
		Iterator<Element> iter = allTasks.iterator();
		ArrayList<TaskModel> taskList = tasks.get(year).get(week);
		while(iter.hasNext()){
			Element element = iter.next();
			Attribute tmpId = element.getAttribute("id");			

			try {				
				if(tmpId.getIntValue() == id){ // is in xml file
					TaskModel taskToStore = null;	
					Iterator<TaskModel> iterArrayList = taskList.iterator();
					while(iterArrayList.hasNext()){
						TaskModel tmp = iterArrayList.next();

						if(tmp.getIdNumber() == id) {							

							element.getChild("title").setText(title);
							element.getChild("content").setText(content);
							element.getChild("location").setText(location);
							element.getChild("category").setText(category);
							element.getChild("priority").setText(priority);
							element.getChild("day").setText(""+day);
							element.getChild("week").setText(""+week);
							element.getChild("month").setText(month);
							element.getChild("year").setText(""+year);
							element.getChild("start-time").setText(""+startTime);
							element.getChild("stop-time").setText(""+stopTime);
							
							tmp.setTitle(title);
							tmp.setContent(content);
							tmp.setLocation(location);
							tmp.setCategory(category);
							tmp.setPriority(priority);
							tmp.setDay(day);
							tmp.setWeekNum(week);
							tmp.setMonth(month);
							tmp.setYear(year);
							tmp.setStartTime(startTime);
							tmp.setStopTime(stopTime);							

							iterArrayList.remove();	
							taskToStore = tmp;
							
							

						}

					}
					if(taskToStore != null){
						taskList.add(taskToStore);
						tasks.get(year).put(week, taskList);	
						storeChangesToFile();
						result = true;
					} else {
						result = false; 
					}
						
				} else {
					System.out.println("task with ID "+id +" was not in xml file");
				}					

			} catch (DataConversionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		return result;
	}
	
	
	
	/**
	 * Delete a task for a given id. Also store the id of the deleted task, so that the id could be reused.
	 * 
	 * @param id, the id of the task to delete
	 * @return true if the task were successfully deleted, false otherwise 
	 */
	@SuppressWarnings("unchecked")
	public boolean deleteTask(int id,int year, int week){
		boolean result = false;
		Element root = getDoc().getRootElement();
		List<Element> allTasks = root.getChildren();
		Iterator<Element> iter = allTasks.iterator();
		ArrayList<TaskModel> taskList = tasks.get(year).get(week);

		while(iter.hasNext()){
			Element element = iter.next();
			Attribute tmpId = element.getAttribute("id");			
			try {				
				if(tmpId.getIntValue() == id){

					Iterator<TaskModel> iterArrayList = taskList.iterator();
					while(iterArrayList.hasNext()){
						TaskModel tmp = iterArrayList.next();

						if(tmp.getIdNumber() == id) {
							iter.remove();
							iterArrayList.remove();

							tasks.get(year).put(week, taskList);

							storeChangesToFile();
							result = true;
						}
					}
				}				
			} catch (DataConversionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}	



	/**
	 * Returns all task for the given week.
	 * 
	 * @param week, the week for the task to get
	 * @return a list of tasks for the given week, null if no tasks for the given week
	 */
	public ArrayList<TaskModel> getTasks(int year, int week){
		ArrayList<TaskModel> list = new ArrayList<TaskModel> (20);

		if(tasks.containsKey(year)) {
			if(tasks.get(year).containsKey(week)) {
				list = tasks.get(year).get(week);
			} 
		}

		return list;
	}




	/**
	 * Save changes made to a JDOM Document to a file.
	 * 
	 * save whatever is in the JDOM Document given by getDoc() to the file given by the path 
	 * return from getPathToXmlFile() method.
	 */
	public void storeChangesToFile(){
		try{

			XMLOutputter outp = new XMLOutputter();			
			Format format = Format.getPrettyFormat();
			outp.setFormat(format);

			// Create file 
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(getPathToXmlFile()),"UTF8"));

			//FileWriter fstream = new FileWriter();
			//BufferedWriter out = new BufferedWriter(fstream);

			outp.output(getDoc(), out);	
			out.close();

		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}



	/**
	 * Prints a JDOM Document in a human-readable format. Will print to the console.  
	 */
	public void printDoc(){
		XMLOutputter outp = new XMLOutputter();
		Format format = Format.getPrettyFormat();
		outp.setFormat(format);
		try {
			outp.output(getDoc(), System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Prints a JDOM Document in a human-readable format. Will print to the console.  
	 */
	public void printDoc(Document doc){
		XMLOutputter outp = new XMLOutputter();
		Format format = Format.getPrettyFormat();
		outp.setFormat(format);
		try {
			outp.output(doc, System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Prints a task in a human-readable format. Will print to the console.  
	 */
	public void printElement(TaskModel task){
		XMLOutputter outp = new XMLOutputter();
		Format format = Format.getPrettyFormat();
		outp.setFormat(format);
		try {
			outp.output(task, System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	public synchronized int getId() {
		int id = 0;
		if(idFile.isFile()){
			Scanner inputStream = null;
			try {				
				inputStream = new Scanner(new FileInputStream(PATH_TO_ID_FILE));
				while(inputStream.hasNextInt()){
					id = inputStream.nextInt();
				}				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return id;
		
	}
	


	public static synchronized void storeIdToFile (int id) {
		PrintWriter outputStream = null;
		if(idFile.isFile()){
			try {							
				outputStream = new PrintWriter(new FileOutputStream(PATH_TO_ID_FILE));
				outputStream.print(id);
				outputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}else {
			try {
				outputStream = new PrintWriter(new FileOutputStream(PATH_TO_ID_FILE));
				outputStream.print(id);
				outputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}



	public static CategoryList getCategories() {
		return categories;
	}


	

}
