package interfaces;

import view.components.EventCard;

public interface UndoRedoInterface {
	
	public EventCard undo(); //gets the newest eventcard in the undo list
	public EventCard redo(); //gets the newest eventcard in the redo list
}
