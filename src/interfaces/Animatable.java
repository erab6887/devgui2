package interfaces;

public interface Animatable {
	
	/**
	 * Will be called when the Animation engine starts the animation. Here should all necessary initializations of the animated target be done. 
	 */
	public void begin();
	
	public void end();
	
	public void animationEvent(float fraction);
	
	public void repeat();
}
