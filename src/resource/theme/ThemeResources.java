package resource.theme;

import java.awt.Color;
import java.awt.Font;
import java.util.ResourceBundle;

public class ThemeResources {
	private static ResourceBundle resourceBundle;
	
	public ThemeResources() {
		resourceBundle = ResourceBundle.getBundle("resource/theme/theme");
	}
	
	public static ResourceBundle getBundle() {
		return resourceBundle;
	}
	
	
	// Colors
	
	
	
	public static Color getEventCardColor(){
		return new Color(Integer.decode(resourceBundle.getString("theme.color.eventcard.color"))); 
	}
	
	public static Color getWeekCardColor(){
		return new Color(Integer.decode(resourceBundle.getString("theme.color.weekcard.color"))); 
	}
	
	public static Color getDayCardColor(){
		return new Color(Integer.decode(resourceBundle.getString("theme.color.daycard.color"))); 
	}

	
	// Fonts
	public static Font getTitleFont() {
		String name = resourceBundle.getString("theme.font.title.name");
		int style = Integer.valueOf(resourceBundle.getString("theme.font.title.style"));
		int size = Integer.valueOf(resourceBundle.getString("theme.font.title.size"));
		return new Font(name, style, size);
	}
	
	public static Font getMenuFont() {
		String name = resourceBundle.getString("theme.font.menu.name");
		int style = Integer.valueOf(resourceBundle.getString("theme.font.menu.style"));
		int size = Integer.valueOf(resourceBundle.getString("theme.font.menu.size"));
		return new Font(name, style, size);
	}
	
	public static Font getControlFont() {
		String name = resourceBundle.getString("theme.font.control.name");
		int style = Integer.valueOf(resourceBundle.getString("theme.font.control.style"));
		int size = Integer.valueOf(resourceBundle.getString("theme.font.control.size"));
		return new Font(name, style, size);
	}
	
	public static Font getSmallFont() {
		String name = resourceBundle.getString("theme.font.small.name");
		int style = Integer.valueOf(resourceBundle.getString("theme.font.small.style"));
		int size = Integer.valueOf(resourceBundle.getString("theme.font.small.size"));
		return new Font(name, style, size);
	}
	
	public static Font getClockFont() {
		String name = resourceBundle.getString("theme.font.clock.name");
		int style = Integer.valueOf(resourceBundle.getString("theme.font.clock.style"));
		int size =  Integer.valueOf(resourceBundle.getString("theme.font.clock.size"));
		return new Font(name, style, size);
	}
	
	public static Font getUserFont() {
		String name = resourceBundle.getString("theme.font.user.name");
		int style = Integer.valueOf(resourceBundle.getString("theme.font.user.style"));
		int size = Integer.valueOf(resourceBundle.getString("theme.font.user.size"));
		return new Font(name, style, size);
	}
	
}
