package resource.language;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 
 * Manages resources such as language and themes.
 *
 */

public class LanguageResources {

	private static Locale defaultLocale; 
	private static ResourceBundle resourceBundle;

	public LanguageResources() {
		defaultLocale = Locale.getDefault();
		resourceBundle = ResourceBundle.getBundle("language/lang", defaultLocale);
	}

	public static ResourceBundle getBundle(){
		return resourceBundle;
	}
	
	public static ResourceBundle changeLocale(Locale locale){
		resourceBundle = ResourceBundle.getBundle("language/lang", locale);
		return resourceBundle;
	}

}
