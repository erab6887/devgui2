package view;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

public class Menu {
	
	private JMenuBar menu;
	private JMenu file, edit, help, window, setTransparency;
	private JMenuItem quit, undo, redo, delete, zeroPercent, twentyfivePercent;
	private JMenuItem fiftyPercent, seventyfivePercent, about, helpCenter;
	
	public Menu() {
		menu = new JMenuBar();
		
		file = new JMenu("File");
		edit = new JMenu("Edit");
		window = new JMenu("Window");
		setTransparency = new JMenu("Set transparency");
		help = new JMenu("Help");
		
		quit = new JMenuItem("Quit");
		quit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4,8));
		undo = new JMenuItem("Undo");
		undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,4));
		redo = new JMenuItem("Redo");
		redo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,1));
		delete = new JMenuItem("Delete task");
		delete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,0));
		zeroPercent = new JMenuItem("0 %");
		zeroPercent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1,4));
		twentyfivePercent = new JMenuItem("25 %");
		twentyfivePercent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2,4));
		fiftyPercent = new JMenuItem("50 %");
		fiftyPercent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3,4));
		seventyfivePercent = new JMenuItem("75 %");
		seventyfivePercent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,4));
		about = new JMenuItem("About");
		about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,4));
		helpCenter = new JMenuItem("Help center");
		helpCenter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1,4));
		
		file.add(quit);
		
		edit.add(undo);
		edit.add(new JSeparator());
		edit.add(redo);
		edit.add(new JSeparator());
		edit.add(delete);
		
		window.add(setTransparency);
		setTransparency.add(zeroPercent);
		setTransparency.add(twentyfivePercent);
		setTransparency.add(fiftyPercent);
		setTransparency.add(seventyfivePercent);
		
		help.add(about);
		help.add(helpCenter);
		
		menu.add(file);
		menu.add(edit);
		menu.add(window);
		menu.add(help);	
	}
	
	public void refresh(){
		menu.repaint();
	}
	
	public JMenuItem getMenuQuit() {
		return quit;
	}
	
	public JMenuItem getMenuRedo() {
		return redo;
	}
	
	public JMenuItem getMenuUndo() {
		return undo;
	}

	public JMenuItem getMenuDelete() {
		return delete;
	}
	
	public JMenuItem getMenuZeroPercent() {
		return zeroPercent;
	}
	
	public JMenuItem getMenuTwentyfivePercent() {
		return twentyfivePercent;
	}
	
	public JMenuItem getMenuFiftyPercent() {
		return fiftyPercent;
	}
	
	public JMenuItem getMenuSeventyfivePercent() {
		return seventyfivePercent;
	}
	
	public JMenuItem getMenuAbout() {
		return about;
	}
	
	public JMenuItem getMenuHelpCenter() {
		return helpCenter;
	}
	
	public JMenuBar getMenubar() {
		return menu;
	}

}
