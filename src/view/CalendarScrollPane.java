package view;

import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import view.components.WeekCard;


public class CalendarScrollPane extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JScrollPane scroller;
	public WeekCard weekCard;

	public CalendarScrollPane() {
		//weekCard = new WeekCard(0, 100, 1160, 1080, 1);
		scroller = new JScrollPane();
	}

	
	@Override
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);
		
		weekCard.draw(g);
	}
	
}
