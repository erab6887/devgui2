package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.CategoryComboBoxModel;
import model.CategoryModel;
import model.XmlTaskModel;

public class CalendarPopup extends JDialog implements ActionListener {

	private JPanel panel;
	private JLabel titleLabel, contentLabel, locationLabel, categoryLabel, priorityLabel;
	private JTextField titleField;
	private JTextArea contentField, locationField;
	private JComboBox categoryList, priorityList;
	private JScrollPane contentScroll, locationScroll;
	private String[] priorities = new String[2];
	private JButton newCategoryButton, saveButton;
	private CalendarCategoryCreator newCategoryDialog;
	private Color newCategoryColor;
	private String newCategoryTitle;
	
	private CategoryComboBoxModel categories;

	public CalendarPopup(JFrame owner, String title, boolean modality) {
		
		super(owner, title, modality);
		this.setMinimumSize(new Dimension(250, 290));
		this.setPreferredSize(new Dimension(250, 290));
		categories = new CategoryComboBoxModel(XmlTaskModel.getCategories().getList());
		
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		
		GridBagConstraints labelConstraints = new GridBagConstraints();
		labelConstraints.fill = GridBagConstraints.BOTH;
		labelConstraints.gridwidth = 1;
		labelConstraints.gridheight = 1;
		labelConstraints.weightx = 0.9;
		labelConstraints.weighty = 0.03; 
		labelConstraints.insets = new Insets(0,20,0,20);
		labelConstraints.anchor = GridBagConstraints.LINE_START;
        
        GridBagConstraints inputConstraints = new GridBagConstraints();
        inputConstraints.fill = GridBagConstraints.BOTH;
        inputConstraints.gridwidth = 1;
        inputConstraints.gridheight = 1;
        inputConstraints.weightx = 0.9;
        inputConstraints.weighty = 0.25; 
        inputConstraints.insets = new Insets(0,20,0,20);
        inputConstraints.anchor = GridBagConstraints.LINE_START;		
		
		titleLabel = new JLabel("Title:");
		titleField = new JTextField();
		
		contentLabel = new JLabel("Content:");
		contentField = new JTextArea();
		contentField.setLineWrap(true);
		contentScroll = new JScrollPane(contentField);
		
		locationLabel = new JLabel("Location:");
		locationField = new JTextArea();
		locationField.setLineWrap(true);
		locationScroll = new JScrollPane(locationField);
		
		categoryLabel = new JLabel("Category:");
		categoryList = new JComboBox(categories);
		
		newCategoryButton = new JButton("Create new category");
		newCategoryButton.addActionListener(this);
		
		priorityLabel = new JLabel("Priority:");
		priorities[0] = "Low";
		priorities[1] = "High";
		priorityList = new JComboBox(priorities);
		priorityList.setSelectedIndex(0);
		
		saveButton = new JButton("Save");
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 0;
		panel.add(titleLabel, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 1;
		panel.add(titleField, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 2;
		panel.add(contentLabel, labelConstraints);
		
		inputConstraints.gridx = 0;
		inputConstraints.gridy = 3;
		panel.add(contentScroll, inputConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 4;
		panel.add(locationLabel, labelConstraints);
		
		inputConstraints.gridx = 0;
		inputConstraints.gridy = 5;
		panel.add(locationScroll, inputConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 6;
		panel.add(categoryLabel, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 7;
		panel.add(categoryList, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 8;
		panel.add(newCategoryButton, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 9;
		panel.add(priorityLabel, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 10;
		panel.add(priorityList, labelConstraints);
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 11;
		panel.add(saveButton, labelConstraints);
		
		this.add(panel);
		
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		
	}
	
	public void setTitle(String s) {
		titleField.setText(s);
	}
	
	public void setContent(String s) {
		contentField.setText(s);
	}
	
	public void setLocation(String s) {
		locationField.setText(s);
	}
	
	public void setCategory(String s) {
		if(s.equals("Work"))
			categoryList.setSelectedIndex(0);
		else if(s.equals("School"))
			categoryList.setSelectedIndex(1);
		else if(s.equals("Home"))
			categoryList.setSelectedIndex(2);
		else
			categoryList.setSelectedIndex(3);
	}
	
	public void setCategory(CategoryModel category) {
		categoryList.setSelectedItem(category);
	}
	
	public void setPriority(String s) {
		if(s.equals("Low"))
			priorityList.setSelectedIndex(0);
		else if(s.equals("High"))
			priorityList.setSelectedIndex(1);
	}
	
	public String getTitleField() {
		return titleField.getText();
	}
	
	public String getContentField() {
		return contentField.getText();
	}
	
	public String getLocationField() {
		return locationField.getText();
	}
	
	public String getCategoryField() {
		int index = categoryList.getSelectedIndex();
		return categories.getElementAt(index).getCategory();
	}
	
	public String getPriorityField() {
		int index = priorityList.getSelectedIndex();
		return priorities[index];
	}
	
	public JButton getSaveButton() {
		return saveButton;
	}

	public void setSelectedIndex(CategoryModel category) {
		categoryList.setSelectedItem(category);
	}
	
	public void setSelectedIndex(int index) {
		categoryList.setSelectedItem(index);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if(newCategoryButton == source) {
			newCategoryDialog = new CalendarCategoryCreator(this, "Custom category", false);
			newCategoryDialog.getSaveButton().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent event)
	            {
					newCategoryColor = newCategoryDialog.getColor();
					newCategoryTitle = newCategoryDialog.getCategoryField();
					newCategoryDialog.dispose();
					
					CategoryModel newCategory = new CategoryModel(
							newCategoryTitle, 
							newCategoryColor.getRed(),
							newCategoryColor.getGreen(), 
							newCategoryColor.getBlue());
					XmlTaskModel.getCategories().addCategory(newCategory);

					categories = new CategoryComboBoxModel(XmlTaskModel.getCategories().getList());
					//categories.addElement(newCategory);
					categoryList.setModel(categories);

					categoryList.setSelectedItem(newCategory);
					
					System.out.println(newCategoryColor+newCategoryTitle);
	            }
	        });
		}
	}
	
}
