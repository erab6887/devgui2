package view;


import helpers.CalendarHelper;
import helpers.Settings;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import model.TaskModel;
import model.XmlTaskModel;

import org.joda.time.DateTimeConstants;

import view.components.DayCard;
import view.components.EventCard;
import view.components.WeekCard;
import view.components.easteregg.Dots;
import view.components.easteregg.FighterJet;
import view.components.easteregg.Missile;

public class CalendarView extends JPanel implements MouseListener,MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4389050771323283260L;
	public EventCard eventCard,eventCard2;
	//public CalendarScrollPane scroller;
	public WeekCard weekCard;
	private CalendarHeader calendarHeader;
	//private Scrollpane 	

	private ArrayList<DayCard> dayCardList;
	private ArrayList<EventCard> eventCardsList;

	int eventCounterID = 0;

	private DayCard monday;
	private DayCard tuesday;
	private DayCard wednesday;
	private DayCard thursday;
	private DayCard friday;
	private DayCard saturday;
	private DayCard sunday;

	private int newEventCardPosX;	
	private int newEventCardPosY;
	private int newEventCardWidth;
	private int newEventCardDay = -1;
	public boolean startedToCreateANewEventCard = false;
	public boolean pressedOnEventCard = false;
	private int eventCardThatIsChanging = -1;  
	private EventCard newEventCard;
	public boolean addedTask = false;

	private volatile int nextId;


	//easter egg stuff
	public FighterJet fighterJet;
	public Dots dots = new Dots();
	
	
	
	public CalendarView(ArrayList<TaskModel> list, int id) {	

		nextId = id;


		//n�r man skapar nya events i ett daycard och f�rs�ker flytta dem s� kan man 
		//inte flytta en utan alla flyttas samtidigt



		this.setFocusable(true); // otherwise the key pressed will not be captured 
		dayCardList = new ArrayList<DayCard>(7);
		eventCardsList = new ArrayList<EventCard>(20); 
		this.setOpaque(false);

		//weekCard = new WeekCard(0, 100, 1160, 540, 1);//sista argumentet ska vara veckans ID/nummer
		//weekCard = new WeekCard(0, 100, 1160, 540, CalendarHelper.getWeekNumber(),list);
		//TODO: fixa s� att allt �r dynamiskt efter bredden (width) (daycard bredden m�ste anpassas efter weekcard)

		//TODO: fixa s� att allt �r dynamiskt efter bredden (width)
		calendarHeader = new CalendarHeader(0, 0, Settings.getFrameWidth(), 100);

		weekCard = new WeekCard(0, 100, Settings.getFrameWidth(), Settings.getWeekCardHeight(), CalendarHelper.getWeekNumber(),list);

		int dayCardYpos  = weekCard.getYpos()+10;
		double dayCardWidth =  Settings.getDayCardWidth();
		int dayCardHeight = Settings.getDayCardHeight();

		System.out.println("CalendarHelper.getWeekNumber()"+CalendarHelper.getWeekNumber());
		System.out.println("CalendarHelper.getWeekNumber()"+CalendarHelper.getWeekNumber());
		System.out.println("CalendarHelper.getWeekNumber()"+CalendarHelper.getYear());
		monday = new DayCard(
				Settings.MONDAY_POS_X,
				dayCardYpos,
				dayCardWidth,
				dayCardHeight,
				"Monday",
				list,
				CalendarHelper.getDate(DateTimeConstants.MONDAY,CalendarHelper.getWeekNumber(),CalendarHelper.getYear()),
				CalendarHelper.monday);		

		tuesday = new DayCard(
				Settings.getDayCardWidth()+Settings.getPaddingBetweenDayCards()+Settings.MONDAY_POS_X, 
				dayCardYpos,
				dayCardWidth,
				dayCardHeight,
				"Tuesday",
				list,
				CalendarHelper.getDate(DateTimeConstants.TUESDAY,weekCard.getWeekNum(),CalendarHelper.getYear()),
				CalendarHelper.tuesday);

		wednesday = new DayCard(
				(Settings.getDayCardWidth()*2+Settings.getPaddingBetweenDayCards()+2+Settings.MONDAY_POS_X), 
				dayCardYpos, 
				dayCardWidth, 
				dayCardHeight, 
				"Wednesday",
				list,
				CalendarHelper.getDate(DateTimeConstants.WEDNESDAY,weekCard.getWeekNum(),CalendarHelper.getYear()),
				CalendarHelper.wednesday);

		thursday = new DayCard(
				(Settings.getDayCardWidth()*3+Settings.getPaddingBetweenDayCards()+4+Settings.MONDAY_POS_X),
				dayCardYpos, 
				dayCardWidth,
				dayCardHeight,
				"Thursday",
				list,
				CalendarHelper.getDate(DateTimeConstants.THURSDAY,weekCard.getWeekNum(),CalendarHelper.getYear()),
				CalendarHelper.thursday);

		friday = new DayCard(
				(Settings.getDayCardWidth()*4+Settings.getPaddingBetweenDayCards()+6+Settings.MONDAY_POS_X),
				dayCardYpos, 
				dayCardWidth,
				dayCardHeight,
				"Friday",
				list,
				CalendarHelper.getDate(DateTimeConstants.FRIDAY,weekCard.getWeekNum(),CalendarHelper.getYear()),
				CalendarHelper.friday);

		saturday = new DayCard(
				(Settings.getDayCardWidth()*5+Settings.getPaddingBetweenDayCards()+8+Settings.MONDAY_POS_X),
				dayCardYpos,
				dayCardWidth,
				dayCardHeight,
				"Saturday",
				list,
				CalendarHelper.getDate(DateTimeConstants.SATURDAY,weekCard.getWeekNum(),CalendarHelper.getYear()),
				CalendarHelper.saturday);

		sunday = new DayCard(
				(Settings.getDayCardWidth()*6+Settings.getPaddingBetweenDayCards()+10+Settings.MONDAY_POS_X),
				dayCardYpos,
				dayCardWidth,
				dayCardHeight,
				"Sunday",
				list,
				CalendarHelper.getDate(DateTimeConstants.SUNDAY,weekCard.getWeekNum(),CalendarHelper.getYear()),
				CalendarHelper.sunday);

		dayCardList.add(monday);
		dayCardList.add(tuesday);
		dayCardList.add(wednesday);
		dayCardList.add(thursday);
		dayCardList.add(friday);
		dayCardList.add(saturday);
		dayCardList.add(sunday);



		Iterator<DayCard> iterDayCard = dayCardList.iterator();
		while(iterDayCard.hasNext()) {
			DayCard tmpDayCard = iterDayCard.next(); 
			Iterator<TaskModel> iter = list.iterator();
			while(iter.hasNext()){
				TaskModel tmpTask = iter.next();
				int dayCardDay = Integer.parseInt(tmpDayCard.getDayCardDay());
				if(dayCardDay == tmpTask.getDay()) {
					System.out.println("start-time:"+tmpTask.getStartTime());
					System.out.println("stop-time:"+tmpTask.getStopTime());
					System.out.println("tmpDayCard.getPosX():"+tmpDayCard.getPosX());
					EventCard tmpEventCard = new EventCard(
							tmpDayCard.getPosX(),
							tmpTask.getStartTime(),
							(int)dayCardWidth,
							tmpTask.getStopTime() - tmpTask.getStartTime(),
							tmpTask.getIdNumber());

					tmpEventCard.setStartTime(tmpTask.getStartTime());
					tmpEventCard.setStopTime(tmpTask.getStopTime());
					tmpEventCard.setCategory(tmpTask.getCategory());
					tmpEventCard.setColor(XmlTaskModel.getCategories().getColor(tmpTask.getCategory()));
					tmpEventCard.setPriority(tmpTask.getPriority());
					tmpEventCard.setContent(tmpTask.getContentTask());
					tmpEventCard.setDay(tmpTask.getDay());
					tmpEventCard.setLocation(tmpTask.getLocation());
					tmpEventCard.setMonth(Integer.parseInt(tmpTask.getMonth()));
					tmpEventCard.setTitle(tmpTask.getTitle());
					tmpEventCard.setWeek(tmpTask.getWeekNum());
					tmpEventCard.setYear(tmpTask.getYear());
					tmpEventCard.setParentDayCardsPosX(tmpDayCard.getPosX());
					eventCardsList.add(tmpEventCard);
				}
			} 
		}
		
		fighterJet = new FighterJet();
	}








	@Override
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);

		//headern ska ritas h�r

		weekCard.draw(g);

		Iterator<DayCard> iterDayCard = dayCardList.iterator();
		while(iterDayCard.hasNext()) {		
			iterDayCard.next().draw(g);		
		}


		iterDayCard = dayCardList.iterator();
		while(iterDayCard.hasNext()) {
			DayCard tmpDayCard = iterDayCard.next(); 
			int tmpDay = Integer.parseInt(tmpDayCard.getDayCardDay());
			Iterator<EventCard> iterEventCard = eventCardsList.iterator();
			while(iterEventCard.hasNext()) {
				EventCard tmpEventCard = iterEventCard.next();				

				if(tmpDay == tmpEventCard.getDay()){
					/*System.out.println("eventCardsList.size(): "+eventCardsList.size());
					System.out.println("DRAW event card with title:"+tmpEventCard.getTitle());
					System.out.println("event with title:"+tmpEventCard.getTitle());
					System.out.println("posX:"+tmpEventCard.getPosX());
					System.out.println("posY:"+tmpEventCard.getPosY());
					System.out.println("width:"+tmpEventCard.getPosX());
					System.out.println("height:"+tmpEventCard.getPosY());*/
					//tmpEventCard.setWidth((int)tmpDayCard.getWidth());
					tmpEventCard.setParentDayCardsPosX(tmpDayCard.getPosX());
					tmpEventCard.draw(g);
				}
			}


		}

		calendarHeader.draw(g);		
		
		fighterJet.draw(g);
		dots.draw(g);	
		
		
		g.dispose();	
	}

	public ArrayList<EventCard> getEventCardsList() {
		return eventCardsList;
	}
	
	private synchronized void incrementId(){		
		this.nextId++;
		new Runnable() {						
			@Override
			public void run() {
				XmlTaskModel.storeIdToFile(nextId);

			}
		}.run();
	}
	
	private synchronized int getId(){
		return this.nextId;
	}

	public void setEventCardsList(ArrayList<EventCard> eventCardsList) {
		this.eventCardsList = eventCardsList;
	}

	@Override
	public void mousePressed(MouseEvent event) {
		
		if(dayCardList.size() > 0) {
			int mouseX = event.getX();
			int mouseY = event.getY();
			System.out.println("mouseX:"+mouseX);
			System.out.println("mouseY:"+mouseY);

			if(eventCardsList.size() > 0) {
				Iterator<EventCard> iterEventCard = eventCardsList.iterator();
				while(iterEventCard.hasNext()) {
					EventCard tmpEventCard = iterEventCard.next();

					if(tmpEventCard.isInsideEventCard(mouseX, mouseY) || tmpEventCard.isOnBottomBorder(mouseX ,mouseY) || tmpEventCard.isOnUpperBorder(mouseX ,mouseY)) {
						startedToCreateANewEventCard = false;
						pressedOnEventCard = true;
						//System.out.println("Class#DayCard : mouse pressed inside event card with title:"+tmpEventCard.getTitle());
						eventCardThatIsChanging = tmpEventCard.getId();
						tmpEventCard.mousePressed(event);
					}
				}
				if(!pressedOnEventCard) {

					Iterator<DayCard> iterDayCard = dayCardList.iterator();
					while(iterDayCard.hasNext()) {
						DayCard tmpDayCard = iterDayCard.next();
						if(tmpDayCard.isInsideDayCard(mouseX, mouseY)) {
							newEventCardPosX = tmpDayCard.getPosX();
							newEventCardPosY = mouseY;
							newEventCardWidth = (int)tmpDayCard.getWidth();
							newEventCardDay = Integer.parseInt(tmpDayCard.getDayCardDay());
						}

					}

					// anv�ndaren tryckte inte p� ett event card, skap ett nytt

					//System.out.println("newEventCardPosX: "+newEventCardPosX);
					//System.out.println("newEventCardPosY: "+newEventCardPosY);
					//System.out.println("newEventCardWidth: "+newEventCardWidth);

					// anv�ndaren tryckte inte p� ett event card, skap ett nytt

					startedToCreateANewEventCard = true;

					//We need to fix this so we do not generate a random id
					/*newEventCard = new EventCard(newEventCardPosX,newEventCardPosY,newEventCardWidth,1,eventCounterID);
					System.out.println("Event created with ID: "+eventCounterID);
					eventCounterID++;*/


					Settings.setEventCardThatToBeAddedToModel(nextId);
					newEventCard = new EventCard(newEventCardPosX,newEventCardPosY,newEventCardWidth,1,getId());
					incrementId();

					//store the id to the file in a separate thread
					



					//newEventCard = new EventCard(500,500,newEventCardWidth,1,1000);
					newEventCard.setTitle("Your title");
					newEventCard.setCategory("Other");
					newEventCard.setColor(XmlTaskModel.getCategories().getColor("Other"));
					newEventCard.setPriority("Low");
					newEventCard.setContent("");
					newEventCard.setLocation("");
					newEventCard.setWeek(CalendarHelper.getWeekNumber());

					newEventCard.setMonth(CalendarHelper.getMonth());
					newEventCard.setYear(CalendarHelper.getYear());
					newEventCard.setDay(newEventCardDay);
					newEventCard.setStartTime(newEventCardPosY);
					newEventCard.setStopTime(newEventCardPosY+1);
				}


			} else {

				Iterator<DayCard> iterDayCard = dayCardList.iterator();
				while(iterDayCard.hasNext()) {
					DayCard tmpDayCard = iterDayCard.next();
					if(tmpDayCard.isInsideDayCard(mouseX, mouseY)) {
						newEventCardPosX = tmpDayCard.getPosX();
						newEventCardPosY = mouseY;
						newEventCardWidth = (int)tmpDayCard.getWidth();
						newEventCardDay = Integer.parseInt(tmpDayCard.getDayCardDay());
					}

				}

				//System.out.println("newEventCardPosX: "+newEventCardPosX);
				//System.out.println("newEventCardPosY: "+newEventCardPosY);
				//System.out.println("newEventCardWidth: "+newEventCardWidth);

				// anv�ndaren tryckte inte p� ett event card, skap ett nytt
				startedToCreateANewEventCard = true;

				/*newEventCard = new EventCard(newEventCardPosX,newEventCardPosY,newEventCardWidth,1,eventCounterID);
				System.out.println("Event created with ID: "+eventCounterID);
				eventCounterID++;*/

				Settings.setEventCardThatToBeAddedToModel(nextId);
				newEventCard = new EventCard(newEventCardPosX,newEventCardPosY,newEventCardWidth,1,getId());
				incrementId();


				//newEventCard = new EventCard(500,500,newEventCardWidth,1,1000);
				newEventCard.setTitle("Your title");
				newEventCard.setContent("");
				newEventCard.setCategory("Other");
				newEventCard.setColor(XmlTaskModel.getCategories().getColor("Other"));
				newEventCard.setPriority("Low");
				newEventCard.setLocation("");
				newEventCard.setWeek(CalendarHelper.getWeekNumber());
				newEventCard.setMonth(CalendarHelper.getMonth());
				newEventCard.setYear(CalendarHelper.getYear());
				newEventCard.setDay(newEventCardDay);
				newEventCard.setStartTime(newEventCardPosY);
				newEventCard.setStopTime(newEventCardPosY+1);
			}


		}


	}

	@Override
	public void mouseReleased(MouseEvent event) {
		// TODO Auto-generated method stub
		if(pressedOnEventCard) {
			if(eventCardsList.size() > 0 ){
				Iterator<EventCard> iter1 = eventCardsList.iterator();
				Iterator<DayCard> iter2 = dayCardList.iterator();
				int mouseX = event.getX();
				int distanceX = Settings.getFrameWidth(), tmpDistanceX = -1; //the positionX of a daycard that is closest to the eventdrop 
				DayCard tmpDayCard1 = null;
				while(iter1.hasNext()) {
					EventCard tmpEventCard = iter1.next();				
					if(tmpEventCard.getId() == eventCardThatIsChanging) {
						while(iter2.hasNext()) {
							DayCard tmpDayCard = iter2.next();

							if(tmpDayCard1 == null){
								tmpDayCard1 = tmpDayCard;
								distanceX = Math.abs((mouseX - (tmpDayCard.getPosX() + ((int)tmpDayCard.getWidth()/2))));
							} else {
								tmpDistanceX = Math.abs((mouseX - (tmpDayCard.getPosX() + ((int)tmpDayCard.getWidth()/2))));
								//tmpPositionX2 = Math.abs((mouseX - tmpDayCard1.getPosX())); 
								if(tmpDistanceX < distanceX) {
									distanceX = tmpDistanceX;
									tmpDayCard1 = tmpDayCard;

								}
							}
						}

						tmpEventCard.setPosX(tmpDayCard1.getPosX());
						tmpEventCard.setDay(Integer.parseInt(tmpDayCard1.getDayCardDay()));
						tmpEventCard.setStopTime(tmpEventCard.getPosY()+tmpEventCard.getHeight());
					}
				}
			} 
		}
		System.out.println("startedToCreateANewEventCard:"+startedToCreateANewEventCard);
		if(startedToCreateANewEventCard){

			Settings.setAddEventToModel(true);
			startedToCreateANewEventCard = false;

		}

		if(pressedOnEventCard) {
			Settings.setPressedOnEventCard(true);
			Settings.setEventCardThatIsChanging(eventCardThatIsChanging);
			pressedOnEventCard = false;
		}

		eventCardThatIsChanging = -1;
		addedTask = false;

		if(eventCardsList.size() > 0 ){
			Iterator<EventCard> iter = eventCardsList.iterator();
			while(iter.hasNext()) {
				EventCard tmpEventCard = iter.next();				
				tmpEventCard.mouseReleased(event);
			}
		} 


	}

	public void createTask() {
		//System.out.println("size event list:"+eventCardsList.size());
		if(!addedTask) {
			eventCardsList.add(newEventCard);
			addedTask = true;
		}

		//System.out.println(":"+eventCardsList.size());
	}

	@Override
	public void mouseDragged(MouseEvent event) {		
		int mouseX = event.getX();
		int mouseY = event.getY();
		//System.out.println("startedToCreateANewEventCard:"+startedToCreateANewEventCard);
		if(weekCard.isInsideWeekCard(mouseX, mouseY)){
			if(startedToCreateANewEventCard) {	

				newEventCard.changeHeight(event.getY() - newEventCard.getHeight() -newEventCard.getPosY());



				newEventCard.mouseDragged(event);
				//newEventCardHeight += (event.getY() - newEventCardHeight -newEventCardPosY);*/
				//System.out.println("Trying to create a new event");
				Runnable createEventcard = new Runnable() { 
					public void run() {
						createTask();	
					} 
				};   
				SwingUtilities.invokeLater(createEventcard);
			} else {
				if(eventCardsList.size() > 0 ){
					Iterator<EventCard> iter = eventCardsList.iterator();
					while(iter.hasNext()) {
						EventCard tmpEventCard = iter.next();
						if(tmpEventCard.getId() ==  eventCardThatIsChanging)
							tmpEventCard.mouseDragged(event);
					}
				} 
			}
		}




	}



	@Override
	public void mouseClicked(MouseEvent event) {

	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseMoved(MouseEvent event) {
		// TODO Auto-generated method stub

		if(eventCardsList.size() > 0 ){
			Iterator<EventCard> iter = eventCardsList.iterator();

			while(iter.hasNext()) {
				if(iter.next().mouseMoved(event)) {
					Settings.setResizeMousePointer(true);
					break;
				} else {
					Settings.setResizeMousePointer(false);
				}
			}
		} 
	}



	public EventCard getNewEventCard() {
		return newEventCard;
	}

	public void setNewEventCard(EventCard newEventCard) {
		this.newEventCard = newEventCard;
	}

	public int getNextId() {
		return nextId;
	}

	public void setNextId(int nextId) {
		this.nextId = nextId;
	}

	public CalendarHeader getCalendarHeader() {
		return calendarHeader;
	}

	public void setCalendarHeader(CalendarHeader calendarHeader) {
		this.calendarHeader = calendarHeader;
	}








	public ArrayList<DayCard> getDayCardList() {
		return dayCardList;
	}






}
