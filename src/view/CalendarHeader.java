package view;

import helpers.CalendarHelper;
import helpers.Settings;
import interfaces.Drawable;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import view.components.ControlPanelHeader;
import view.components.CustomButton;



public class CalendarHeader implements Drawable{

	private int posX;
	private int posY;
	private int width;
	private int height;

	private int posXButton = (int)(Settings.getFrameWidth()-160);
	private int posYButton = (int)(Settings.getFrameHeight()*0.03);
	private int posXDate = 20;
	private int posYDate = (int)(Settings.getFrameHeight()*0.10);

	private final static int WIDTH_OF_BUTTON = 120;
	private final static int HEIGHT_OF_BUTTON = 30;
	
	CustomButton controlPanelButton;
	int widthOfControlPanel;
	int heightOfControlPanel;
	private int posXOfControlPanel;
	private int posYOfControlPanel;
	private int posXOfControlPanelText;
	private int posYOfControlPanelText;
	
	String startDate;	
	String endDate;
	String weekNum;		
	String startMonth;
	String endMonth;
	String startYear;
	String endYear;
	
	private ControlPanelHeader controlPanel;
	public boolean isControlPanelVisible = false;

	public CalendarHeader(int posX, int posY, int width, int height) {

		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
		Settings.setHeaderHeight(height);
		controlPanel = new ControlPanelHeader(0, 0);
		
		posXOfControlPanel = (int) (width/2);
		posYOfControlPanel = (int) (height/3);
		widthOfControlPanel = WIDTH_OF_BUTTON;
		heightOfControlPanel = HEIGHT_OF_BUTTON;
		

		posXOfControlPanelText = posXOfControlPanel - (widthOfControlPanel/5);
		posYOfControlPanelText = posYOfControlPanel;
		
		controlPanelButton = new CustomButton(widthOfControlPanel, heightOfControlPanel, posXOfControlPanel, posYOfControlPanel, "Highlight");
		controlPanelButton.createToolTip("This panel could be used to filter your events");

		controlPanelButton.setColor(Color.LIGHT_GRAY);
		controlPanelButton.setButtonText(posXOfControlPanelText, posYOfControlPanelText);
	}

	@Override
	public void draw(Graphics g) {

		Graphics2D gTemp = (Graphics2D)g.create();

		this.width = Settings.getHeaderWidth();
		this.height = Settings.getHeaderHeight();
		posXButton = (int)(Settings.getFrameWidth()-160);
		posYButton = (int)(Settings.getHeaderHeight()/3);
		posXDate = 20;
		posYDate = (int)(Settings.getFrameHeight()*0.10);


		//To remove the jagged corners of the EventCard
		RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		renderHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		gTemp.setRenderingHints(renderHints);

		Image headerBackground = null;

		try{
			headerBackground = ImageIO.read(new File("src/resource/image/header.png"));
		}	catch (IOException ex) {
			System.out.println("Error: Header background not found");
		}

		gTemp.drawImage(headerBackground, 0, 0, width, height, null);
		
		gTemp.setColor(Color.LIGHT_GRAY);
		gTemp.fillRoundRect(posXButton, posYButton, WIDTH_OF_BUTTON, HEIGHT_OF_BUTTON, 15, 15);

		gTemp.setColor(Color.BLACK);
		gTemp.drawLine(posXButton+30, posYButton, posXButton+30, posYButton+29);
		gTemp.drawLine(posXButton+90, posYButton, posXButton+90, posYButton+29);

		Font font = new Font("Serif", Font.BOLD, 12);
		gTemp.setFont(font);

		gTemp.drawString("This Week", posXButton+32, posYButton+20);

		Image arrowLeft = null;
		Image arrowRight = null;

		try{
			arrowLeft = ImageIO.read(new File("src/resource/image/arrow_left_small.png"));
		}	catch (IOException ex) {
			System.out.println("Error: Left arrow not found");
		}

		try{
			arrowRight = ImageIO.read(new File("src/resource/image/arrow_right_small.png"));
		}	catch (IOException ex) {
			System.out.println("Error: Right arrow not found");
		}

		gTemp.drawImage(arrowLeft, posXButton+8, posYButton+5, null);
		gTemp.drawImage(arrowRight, posXButton+98, posYButton+5, null);

		Font dateFont = new Font("Serif", Font.BOLD, 16);
		gTemp.setFont(dateFont);
		
		startDate = ""+CalendarHelper.getStartDay();
		endDate = ""+CalendarHelper.getEndDay();
		weekNum = ""+CalendarHelper.getWeekNumber();		
		startMonth = CalendarHelper.getMonthAsString(CalendarHelper.getStartMonth());
		endMonth = CalendarHelper.getMonthAsString(CalendarHelper.getEndMonth());
		startYear = ""+CalendarHelper.getStartYear();
		endYear = ""+CalendarHelper.getEndYear();		

		String dateString = startDate + " " + startMonth + " " + startYear + " - " + endDate + " " + endMonth + " " + endYear;
		int posStringX = (int) (posXButton * 0.5);

		gTemp.drawString(dateString, posXDate, posYDate);

		//**********************start with the Control Panel button
		posXOfControlPanel = width/2;
		posYOfControlPanel = height/3;
		
		controlPanelButton.setX((int)(Settings.getFrameWidth()/2));
		controlPanelButton.setY((int)(Settings.getHeaderHeight()/3));
		
		posXOfControlPanelText = controlPanelButton.getX();
		posYOfControlPanelText = controlPanelButton.getY();
		
		controlPanelButton.setButtonTextPosX((int)((Settings.getFrameWidth()/2) + 27));
		controlPanelButton.setButtonTextPosY((int)(Settings.getHeaderHeight()/3)+20);
		
		gTemp.setColor(Color.LIGHT_GRAY);
		gTemp.setColor(Color.BLACK);
		controlPanelButton.draw(gTemp);
				
		//The Control Panel button
		if(isControlPanelVisible) {
			controlPanel.setControlPanelHeight(HEIGHT_OF_BUTTON);
			controlPanel.setControlPanelWidth(WIDTH_OF_BUTTON);
			controlPanel.draw(gTemp);
		} else {
			controlPanel.setControlPanelHeight(0);
			controlPanel.setControlPanelWidth(0);
		}
		
		
	}

	public boolean isInsideLeftButton(int mouseX, int mouseY) {
		boolean res =  false;
		if(mouseX >= posXButton && mouseX <= (posXButton+30)  && mouseY >= posYButton && mouseY <= (posYButton+HEIGHT_OF_BUTTON)) {
			res = true;
		}
		return res;
	}

	public boolean isInsideMiddleButton(int mouseX, int mouseY) {
		boolean res =  false;
		if(mouseX > (posXButton+30)  && mouseX < (posXButton+90)  && mouseY >= posYButton && mouseY <= (posYButton+HEIGHT_OF_BUTTON)) {
			res = true;
		}
		return res;
	}

	public boolean isInsideRightButton(int mouseX, int mouseY) {
		boolean res =  false;
		if(mouseX >= (posXButton+90)  && mouseX < (posXButton+WIDTH_OF_BUTTON)  && mouseY >= posYButton && mouseY <= (posYButton+HEIGHT_OF_BUTTON)) {
			res = true;
		}
		return res;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(String weekNum) {
		this.weekNum = weekNum;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}
	
	public ControlPanelHeader getControlPanel() {
		return controlPanel;
	}

	public CustomButton getControlPanelButton() {
		return controlPanelButton;
	}

	

	
}
