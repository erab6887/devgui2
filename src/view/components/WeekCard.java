package view.components;

import helpers.CalendarHelper;
import helpers.Settings;
import interfaces.Animatable;
import interfaces.Drawable;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

import model.TaskModel;

import org.joda.time.DateTimeConstants;

import controller.Actions;

import resource.theme.ThemeResources;

public class WeekCard implements Drawable, Animatable{

	private int posX;
	private int posY;
	private int width;
	private int height;
	private int weekNum;
	private String timeAM = "AM";
	private String timePM = "PM";
	private String noon = "Noon";

	private int hoursPerDay = 12;

	public static int timeLine;
	public static int nrOfPixelsPerHour;

	//Daycards

	/**
	 * 
	 * @param posX, x-coordinate for the DayCards top-left corner 
	 * @param posY, y-coordinate for the DayCards top-left corner
	 * @param width, the DayCards width
	 * @param height, the DayCards height
	 */

	public WeekCard(int posX, int posY, int width, int height, int id,ArrayList<TaskModel> list){

		this.posX = posX;
		this.posY = posY;
		this.width = Settings.getFrameWidth();
		this.height =  Settings.getWeekCardHeight();//Settings.getFrameHeight()-Settings.getHeaderHeight()-25; // -22 for handling the size of title bar of the frame

		//System.out.println("weekFramheight:"+Settings.getWeekCardHeight());

		this.weekNum = id;
		//System.out.println("Week number of WeekCard: "+weekNum);


	}

	//h�ller bara koll p� sina daycards och var dem ska hamna i vyn,
	//vidarebefordrar �ven en lista med events fr�n calenderviewn till daycardsen

	@Override
	public void draw(Graphics g) {	

		//System.out.println("draw week card");
		this.height = Settings.getWeekCardHeight();//Settings.getFrameHeight()-Settings.getHeaderHeight()-25;
		this.width = Settings.getFrameWidth();
		this.posY = Settings.getWeekCardPosY();
		Graphics2D gTemp = (Graphics2D)g.create();
		//give it some colour
		gTemp.setColor(ThemeResources.getWeekCardColor());

		//To remove the jagged corners of the EventCard
		RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		renderHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		gTemp.setRenderingHints(renderHints);
		gTemp.fillRect(posX,posY, width, height);

		/*monday.draw(gTemp); //add another argument, the date for this day so it can be drawn
		tuesday.draw(gTemp);
		wednesday.draw(gTemp);
		thursday.draw(gTemp);
		friday.draw(gTemp);
		saturday.draw(gTemp);
		sunday.draw(gTemp);
		 */
		gTemp.setColor(Color.BLACK);
		//gTemp.setColor(ThemeResources.getDayCardColor());
		//int timeLine = ((height + posY) % height) + 75;
		timeLine = posY + 75;
		nrOfPixelsPerHour = (int)Math.floor(((height - 60 )/24));//height/27;

		/*System.out.println("NUMBER OF PIXELS PER HOUR: " + nrOfPixelsPerHour);
		System.out.println("HEIGHT: " + nrOfPixelsPerHour * 24);
		System.out.println("HEIGHT2: " + (height - 60));*/
		//System.out.println(timeLine);
		//System.out.println(timeLine+height-100);

		Font font = new Font("Arial", Font.PLAIN, 11);
		//every whole hour
		int j = 0;
		for(int i = timeLine; i<(height+posY-20); i=i+nrOfPixelsPerHour) {
			if(j < 24 ) {
				gTemp.setFont(font);
				gTemp.drawString(j + ".00", posX + 10, i);
			}
			j++;
		}

		gTemp.dispose();

	}

	public int getYpos() {
		return posY;
	}

	public int getXpos() {
		return posX;
	}

	//retrieve dates from the XML model to be able to print them on the daycards
	public int getWeekDates() {
		return 10;

	}

	public boolean isInsideWeekCard(int mouseX, int mouseY) {
		boolean res = false;
		if(mouseX >= posX && mouseX <= (posX+width-1) && mouseY >= posY && mouseY <= (posY+height-1)) {
			res = true;
		}
		return res;
	}




	public int getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(int weekNum) {
		this.weekNum = weekNum;
	}

	@Override
	public void begin() {
		// TODO Auto-generated method stub

	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public void animationEvent(float fraction) {
		width = (int)Math.abs((Settings.getFrameWidth() - (Settings.getFrameWidth()* 2*fraction)));
		Actions.view.repaint();

	}

	@Override
	public void repeat() {
		// TODO Auto-generated method stub
		
	}



}

