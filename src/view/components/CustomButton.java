package view.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import helpers.Settings;
import interfaces.Drawable;

public class CustomButton implements Drawable{

	private int width;
	private int height;
	private int x;
	private int y;
	private String buttonText;
	private int buttonTextPosX;
	private int buttonTextPosY;
	private Color color;
	
	private boolean buttonPressed = false;
	private ToolTip toolTip = null;
	
	public CustomButton(int width, int height, int posX, int posY, String name) {
		this.width = width;
		this.height = height;
		x = posX;
		y = posY;
		buttonText = name;
		System.out.println("WIDTH OF CUSTOM BUTTON:"+width);
		buttonTextPosX = (x + (width/3));
		buttonTextPosY = (int) (y + (height/1.5));
	}

	@Override
	public void draw(Graphics g) {
		
		Graphics2D gTemp = (Graphics2D)g.create();
		
		if(buttonText.equalsIgnoreCase("Highlight")) {
			Image controlImg = null; 
			
			try{
				controlImg = ImageIO.read(new File("src/resource/image/controlpanelButton.png"));
			}	catch (IOException ex) {
				System.out.println("Error: Control button image not fund");
			}

			gTemp.drawImage(controlImg,Settings.getFrameWidth()/2, Settings.getHeaderHeight()/3, 120, 30, null);
			
		} else {
		
			gTemp.setColor(color);
			gTemp.fillRoundRect(x, y, width, height, 15, 15);
			if(buttonPressed) {
				gTemp.setColor(Color.RED);
				gTemp.drawRoundRect(x+1, y+1, width-3, height-3, 15, 15);
			}
		}
		
		if(toolTip != null) {
			
			toolTip.setPosXToolTipBox((x+width+1));
			toolTip.setPosYToolTipBox(y-25);
			toolTip.draw(g);
		}
		
		gTemp.setColor(Color.BLACK);
		gTemp.drawString(buttonText, buttonTextPosX, buttonTextPosY);
		
	}
	
	public boolean isInsideButton(int mouseX, int mouseY) {
		if(mouseX >= x && mouseX <= (x + width) && mouseY >= y && mouseY <= (y + height)) {
			return true;
		}
		
		return false;
		
	}
	
	
	public void createToolTip(String text) {
		toolTip = new ToolTip((x+width+1),y-25,(int)(width*0.7),(int)(height*3));
		toolTip.setText(text);
	}
	
	public void setToolTipText(String text) {
		if(toolTip != null) {
			toolTip.setText(text);
		}
	}
	
	public void mouseEntered(int mouseX,int mouseY) {		
		if(isInsideButton(mouseX, mouseY)) {
			if(!toolTip.isVisible())
				toolTip.startTimer();
		}
	}
	
	public void mouseLeaved(int mouseX,int mouseY) {
		if(!isInsideButton(mouseX, mouseY)) {
			//System.out.println("MOUSE LEAVED EVENTCARD");
			toolTip.stopTimer();
		}
	}
	
	public void mouseMoved(MouseEvent event) {
		int mouseY = event.getY();
		int mouseX = event.getX();
		
		mouseEntered(mouseX, mouseY);
		mouseLeaved(mouseX, mouseY);
	}
	
	public boolean isButtonPressed() {
		return buttonPressed;
	}

	public void setButtonPressed(boolean buttonPressed) {
		this.buttonPressed = buttonPressed;
	}

	public void setButtonText(int x, int y) {
		buttonTextPosX = (x + (width/3));
		buttonTextPosY = (int) (y + (height/1.5));;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getButtonTextPosX() {
		return buttonTextPosX;
	}

	public void setButtonTextPosX(int buttonTextPosX) {
		this.buttonTextPosX = buttonTextPosX;
	}

	public int getButtonTextPosY() {
		return buttonTextPosY;
	}

	public void setButtonTextPosY(int buttonTextPosY) {
		this.buttonTextPosY = buttonTextPosY;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
	
	
}
