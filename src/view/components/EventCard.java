package view.components;

import helpers.Settings;
import helpers.WordUtils;
import helpers.p;

import interfaces.Animatable;
import interfaces.Drawable;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;



import controller.Actions;

public class EventCard implements Drawable,Animatable{

	private int posX;
	private int posY;
	private int width;
	private int height;
	private int upperBorderCord = this.posY;
	private int bottomBorderCord = this.height+this.posY;


	private int id;
	private int day;
	private int week;
	private String monthString;
	private int month;
	private int year;

	private int startTime;
	private int stopTime;
	private String title = null;
	private String content = null;
	private String location = null;
	private String category = null;
	private String priority = null;
	private Color color;


	private boolean isInside = false;
	private boolean isInsideOnMousePressed = false;
	private boolean isOnUpperBorder = false;
	private boolean isOnBottomBorder = false;
	private boolean resizeable =  false;
	private boolean resizeUpper = false;
	private boolean resizeBottom = false;
	private boolean isClicked = false;

	private int parentDayCardsPosX;

	private boolean isTranslucent = false;

	

	/*--------------Animation stuff--------------*/
	private int alpha = 230;
	private boolean isAnimating = false;
	private float zoom;
	private double rotate;
	private int tmpPosX;
	private int tmpWidth;

	/**
	 * 
	 * @param posX, x-coordinate for the EventCards top-left corner 
	 * @param posY, y-coordinate for the EventCards top-left corner
	 * @param width, the EventCards width
	 * @param height, the EventCards height
	 * @param id, id number of the EventCard 
	 */
	public EventCard(int posX, int posY, int width, int height,int id){
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.tmpWidth = width; 
		this.height = height;
		this.tmpPosX = posX;
		this.upperBorderCord = this.posY;
		this.bottomBorderCord = this.height+this.posY;
		this.id = id;
		
	}



	@Override
	public void draw(Graphics g) {	

		Graphics2D gTemp = (Graphics2D)g.create();	
		Color c = null;

		

		if(isTranslucent) {			 
			Color ct = new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha-100);
			gTemp.setColor(ct);
		} else {
			if(color != null) {
				Color ct = new Color(color.getRed(),color.getGreen(),color.getBlue(),alpha);
				int red = ct.getRed();
				int green = ct.getGreen();
				int blue = ct.getBlue();
				float[] hsv = new float[3];
				Color.RGBtoHSB(red, green, blue, hsv);

				float hue = hsv[0];
				float saturation = hsv[1];
				float brightness = hsv[2];
				
				if(isClicked) {
					brightness = brightness - 0.25f;
					brightness = Math.max(brightness, 0.1f);
					
					//System.out.println("hue = "+hue + " , saturation = "+ saturation+ " , brightness = "+brightness);

					int rgb = Color.HSBtoRGB(hue, saturation, brightness);
					Color tmp = new Color(rgb);
					Color clicked = new Color(tmp.getRed(),tmp.getGreen(),tmp.getBlue(),alpha-20);

					gTemp.setColor(clicked);

				} else {
					//System.out.println("saturation" + saturation + " hue" + hue + " brightness" + brightness);
					brightness = Math.max(brightness, 0.50f);
					int rgb = Color.HSBtoRGB(hue, saturation, brightness);
					Color tmp = new Color(rgb);
					//System.out.println("ALPHA:"+alpha);
					gTemp.setColor(new Color(tmp.getRed(),tmp.getGreen(),tmp.getBlue(),alpha));				
				}
			}

		}

		if(!isAnimating) {			
			width = Settings.getDayCardWidth();
		} else {
			gTemp.translate(0,0);

			//gTemp.scale(zoom, zoom);
			//gTemp.translate(350,300);
			//gTemp.scale(zoom, zoom);
			//gTemp.rotate(rotate);
			gTemp.rotate(rotate);
		}

		//To remove the jagged corners of the EventCard
		RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		renderHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		gTemp.setRenderingHints(renderHints);

		
		gTemp.fillRoundRect(posX,posY, width, height, 10, 10);

		gTemp.setColor(Color.BLACK);

		String tempTitle = title;
		//if(tempTitle.length() > 15)
			//tempTitle = tempTitle.substring(0, 14) + "...";

		Font text = new Font("Helvetica", Font.PLAIN, 13);
		gTemp.setFont(text);
		

		
		int wrapColumns = (int)Math.ceil((width*0.145));
		tempTitle = WordUtils.wrap(tempTitle,wrapColumns , null, true);		
		int lineHeightSpace = 15;
		int nextLine = posY+20;
		for (String line : tempTitle.split("\n")) {
			gTemp.drawString(line, posX+3, nextLine);
			nextLine = nextLine + lineHeightSpace;
		}
		
		gTemp.dispose();
	}


	
	

	

	public void mousePressed(MouseEvent event) {
		int mouseX = event.getX();
		int mouseY = event.getY();
																														

		isInsideOnMousePressed = isInsideEventCard(mouseX,mouseY);

		// if inside of the left and right border of this event card
		boolean isInSideWidth = (mouseX >= posX && mouseX <= (posX+width-1));

		if (isOnUpperBorder(mouseX ,mouseY)) {// && isInSideWidth){ // is on the upper border of the event card
			resizeable = true;
			resizeUpper = true;
			resizeBottom = false;
		} else if (isOnBottomBorder(mouseX ,mouseY)) {// && isInSideWidth) {
			resizeable = true;
			resizeUpper = false;
			resizeBottom = true;
		} else {
			resizeable = false;
			resizeUpper = false;
			resizeBottom = false;
		}

		//System.out.println("newEventCardPosY:"+newEventCardPosY+ ", bottom border:"+ bottomBorderCord + ", upper border:"+ posY);
		//System.out.println("resizeable:"+resizeable+ ", resizeBottom:"+ resizeBottom + ", resizeable:"+ resizeable);

		if(!resizeable) {
			if(isInSideWidth && mouseY >= posY && mouseY <= (posY+height-1)) {
				System.out.println("Mouse pressed in side a event card :"+id);
				isInsideOnMousePressed = true;
				isTranslucent = true;
			} else {
				isInsideOnMousePressed = false;

				//System.out.println("mouse pressed and event card is to be created");
				//make new event card;
				//int tmpPosX = mouseX;
				//int tmpPosY
				//new EventCard();
			}
		} 

	}

	public void mouseDragged(MouseEvent event) {

		int mouseY = event.getY();
		if(resizeable) {


			if(resizeUpper) {	
				int prevPosY = posY;
				posY = mouseY;
				height = (height+prevPosY-posY);

			} else if (resizeBottom) {
				height += (mouseY - height -posY);		
			}
		} else {
			if(isInsideOnMousePressed) {
				int eventCardCenterX = width / 2;
				int eventCardCenterY = height / 2;
				posX = event.getX() - eventCardCenterX;
				posY = event.getY() - eventCardCenterY;			
				//upperBorderCord = posY;

			}
		}
		upperBorderCord = posY;
		bottomBorderCord = height+posY;
		/*System.out.println("DRAGGED height"+height);
		System.out.println("DRAGGED height"+height);
		System.out.println("DRAGGED bottomBorderCord"+bottomBorderCord);*/
		//System.out.println("newEventCardPosY:"+newEventCardPosY+ ", bottom border:"+ bottomBorderCord + ", upper border:"+ upperBorderCord);
	}
	public void mouseReleased(MouseEvent event) {

		isInsideOnMousePressed = false;
		resizeable = false;
		isTranslucent = false;
	}




	public boolean mouseMoved(MouseEvent event) {
		int mouseY = event.getY();
		int mouseX = event.getX();
		
	
		
		if (isOnUpperBorder(mouseX ,mouseY) || isOnBottomBorder(mouseX ,mouseY)){ 
			//System.out.println("on border");// is on the upper border of the event card
			//Settings.setResizeMousePointer(true);
			return true;

		} else {
			//System.out.println("not on border");
			//Settings.setResizeMousePointer(false);
			return false;
		}
	}

	public boolean isInsideEventCard(int mouseX, int mouseY) {
		boolean res = false;
		if(mouseX >= posX && mouseX <= (posX+width-1) && mouseY >= posY && mouseY <= (posY+height-1)) {
			res = true;
		}
		return res;
	}

	public void isClicked(int mouseX, int mouseY) {
		isClicked = isInsideEventCard(mouseX, mouseY);
	}

	public boolean isOnUpperBorder(int mouseX , int mouseY){

		//System.out.println("upperBorderCord:"+upperBorderCord +", mouseY:"+mouseY);

		if(mouseY >= upperBorderCord-2 && mouseY <= upperBorderCord+2 && mouseX >= posX && mouseX <= (posX+width-1) ) { 
			isOnUpperBorder =  true;
			return true;
		} 
		isOnUpperBorder = false;
		return false;
	}

	public boolean isOnBottomBorder(int mouseX , int mouseY){

		//System.out.println("bottomBorderCord:"+bottomBorderCord +", mouseY:"+mouseY);
		if(mouseY >= bottomBorderCord-2 && mouseY <= bottomBorderCord+2 && mouseX >= posX && mouseX <= (posX+width-1) ) { 
			isOnBottomBorder = true;
			return true;
		}
		isOnBottomBorder = false;
		return false;
	}

	public int getDay() {
		return day;
	}



	public void setDay(int day) {
		this.day = day;
	}



	public int getWeek() {
		return week;
	}



	public void setWeek(int week) {
		this.week = week;
	}



	public int getMonth() {
		return month;
	}



	public void setMonth(int month) {
		this.month = month;
		switch(month) {
		case 1 : 	this.monthString = "January";

		break;
		case 2 : 	this.monthString = "February";
		break;
		case 3 : 	this.monthString = "March";
		break;
		case 4 : 	this.monthString = "April";
		break;
		case 5 :	this.monthString = "May";
		break;
		case 6 :	this.monthString = "June";
		break;
		case 7 :	this.monthString = "July";
		break;
		case 8 : 	this.monthString = "August";
		break;
		case 9 : 	this.monthString = "September";
		break;	
		case 10: 	this.monthString = "October";
		break;
		case 11: 	this.monthString = "November";
		break;
		case 12: 	this.monthString = "December";
		break;
		default: 	this.monthString = "Invalid monthString ";
		break;
		}
	}

	public EventCard clone() {
		EventCard result = new EventCard(
				this.getPosX(),
				this.getPosY(), 
				this.getWidth(),
				this.getHeight(),this.getId());

		result.setStartTime(this.getStartTime());
		result.setStopTime(this.getStopTime());
		result.setCategory(this.getCategory());
		result.setColor(this.color);
		result.setContent(this.getContent());
		result.setDay(this.getDay());
		result.setLocation(this.getLocation());
		result.setMonth(this.getMonth());
		result.setTitle(this.getTitle());
		result.setWeek(this.getWeek());
		result.setYear(this.getYear());
		result.setParentDayCardsPosX(this.getPosX());
		return result;

	}


	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public int getUpperBorderCord() {
		return upperBorderCord;
	}

	public void setUpperBorderCord(int upperBorderCord) {
		this.upperBorderCord = upperBorderCord;
	}

	public int getBottomBorderCord() {
		return bottomBorderCord;
	}

	public void setBottomBorderCord(int bottomBorderCord) {
		this.bottomBorderCord = bottomBorderCord;
	}



	public int getHeight() {
		return height;
	}


	public void changeHeight(int toChange) {
		this.height +=toChange;
	}

	public void setHeight(int height) {
		this.height = height;
	}



	public int getPosX() {
		return posX;
	}



	public void setPosX(int posX) {
		this.posX = posX;
	}



	public int getPosY() {
		return posY;
	}



	public void setPosY(int posY) {
		this.posY = posY;
	}



	public int getWidth() {
		return width;
	}



	public void setWidth(int width) {
		this.width = width;
	}



	public int getStartTime() {
		return startTime;
	}



	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}



	public int getStopTime() {
		return stopTime;
	}



	public void setStopTime(int stopTime) {
		this.stopTime = stopTime;
	}



	public int getParentDayCardsPosX() {
		return parentDayCardsPosX;
	}



	public void setParentDayCardsPosX(int parentDayCardsPosX) {
		this.parentDayCardsPosX = parentDayCardsPosX;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getMonthString() {
		return monthString;
	}




	public Color getColor() {
		return color;
	}



	public void setColor(Color eventCardColor) {
		this.color = eventCardColor;
	}



	@Override
	public void begin() {



	}

	@Override
	public void repeat() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void end() {
		System.out.println("ANIMATING HAS ENDED");
		isAnimating = false;

	}

	

	public boolean isClicked() {
		return isClicked;
	}



	@Override
	public void animationEvent(float fraction) {
		isAnimating = true;
		//System.out.println("FRACTION:"+fraction);
		//alpha = (int)Math.abs(240-(240* 2*fraction));
		System.out.println("FRACTION:"+fraction);
		alpha = (int)(240-(200*fraction));
		//System.out.println("ALPHA:"+alpha);

		//width = (int)Math.abs(Settings.getDayCardWidth() - (Settings.getDayCardWidth() * 2* fraction));

		
		//zoom = Math.abs(1-(2*fraction));
		//System.out.println("ZOOM:"+zoom);
		
		//rotate = Math.abs(2*Math.PI - (2*Math.PI * fraction));
		System.out.println("ROTATE:"+rotate);

		//posX = Math.abs((int)(tmpPosX-(2*tmpPosX*fraction)));
		//posX = Math.abs((int)(tmpPosX-(10*fraction)));
		//width = Math.abs((int)(tmpWidth+(10*fraction)));
		//zoom = Math.abs(1-(2*fraction));
		//System.out.println("ZOOM:"+zoom);

		//rotate = Math.abs(2*Math.PI - (2*Math.PI * fraction));
		//System.out.println("ROTATE:"+rotate);

		Actions.view.repaint();

	}



	







}
