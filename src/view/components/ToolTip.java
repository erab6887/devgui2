package view.components;

import helpers.AnimationEngine;
import helpers.StringUtils;
import interfaces.Animatable;
import interfaces.Drawable;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.swing.Timer;

import controller.Actions;

public class ToolTip implements Drawable , Animatable,ActionListener{


	private boolean visible = false;

	private int  posXToolTipBox;
	private int  posYToolTipBox;
	private int  widthToolTipBox;
	private int  heightToolTipBox;

	private Timer timer;
	private int timerDelay = 1000;

	private int alpha = 220;
	private Color color;

	private AnimationEngine engine;
	private boolean animating;
	private boolean firstTime = false;
	private float zoom;
	private int tmpWidth;
	private int tmpHeight;
	
	private String toolTipText =  "" ;

	private AffineTransform at = new AffineTransform();

	public ToolTip(int x, int y, int width, int height) {
		posXToolTipBox = x;
		posYToolTipBox = y;
		widthToolTipBox = width;
		heightToolTipBox = height;
		
		timer = new Timer(timerDelay, this);
		color = new Color(249,226,154,alpha);
		engine = new AnimationEngine(500, this);
		engine.setPowerOfN(5);
		engine.setEaseInOut();
		tmpWidth = width;
		tmpHeight = height;
		widthToolTipBox = 0;
		heightToolTipBox = 0;
	}

	@Override
	public void draw(Graphics g) {		


	


		if(visible) {

			Graphics2D gTemp = (Graphics2D)g.create();
			
			

			//To remove the jagged corners of the EventCard
			/*RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
			renderHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_SPEED);
			gTemp.setRenderingHints(renderHints);
*/
			gTemp.setColor(color);
			gTemp.fillRoundRect(posXToolTipBox,posYToolTipBox, widthToolTipBox, heightToolTipBox, 10, 10);

			gTemp.setColor(Color.BLACK);
			Font font = new Font("Arial", Font.PLAIN, 12);
			gTemp.setFont(font);			
			StringUtils.drawString(gTemp,toolTipText, posXToolTipBox + 5,posYToolTipBox+15,widthToolTipBox);
			
			
			gTemp.dispose();
		}
	}

	
	


	public void startTimer(){
		
		timer.start();
		firstTime = true;
	}

	public void stopTimer(){
		visible = false;
		firstTime = true;		
		engine.stop();
		timer.stop();
		Actions.view.repaint();
	}

	@Override
	public void begin() {
		
		visible = true;

	}

	@Override
	public void end() {
		animating = false;
	

	}

	@Override
	public void animationEvent(float fraction) {
		animating = true;
		widthToolTipBox = (int)(tmpWidth * fraction);
		heightToolTipBox = (int)(tmpHeight * fraction);
		
		Actions.view.repaint(posXToolTipBox,posYToolTipBox,widthToolTipBox,heightToolTipBox);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(firstTime) {			
			engine.start();			
			firstTime = false;
		}
		
		Actions.view.repaint(posXToolTipBox,posYToolTipBox,widthToolTipBox,heightToolTipBox);
	}

	public int getPosXToolTipBox() {
		return posXToolTipBox;
	}

	public void setPosXToolTipBox(int posXToolTipBox) {
		this.posXToolTipBox = posXToolTipBox;
	}

	public int getPosYToolTipBox() {
		return posYToolTipBox;
	}

	public void setPosYToolTipBox(int posYToolTipBox) {
		this.posYToolTipBox = posYToolTipBox;
	}

	public int getWidthToolTipBox() {
		return widthToolTipBox;
	}

	public void setWidthToolTipBox(int widthToolTipBox) {
		this.widthToolTipBox = widthToolTipBox;
	}

	public int getHeightToolTipBox() {
		return heightToolTipBox;
	}

	public void setHeightToolTipBox(int heightToolTipBox) {
		this.heightToolTipBox = heightToolTipBox;
	}

	public int getTimerDelay() {
		return timerDelay;
	}

	public void setTimerDelay(int timerDelay) {
		this.timerDelay = timerDelay;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}


	public void setColor(int r, int g, int b) {
		this.color = new Color(r,g,b);
	}

	public void setColor(int r, int g, int b,int a) {
		this.color = new Color(r,g,b,a);
	}


	public boolean isAnimating() {
		return animating;
	}

	public void setAnimating(boolean animating) {
		this.animating = animating;
	}

	public void setText(String text) {
		toolTipText = text;
	}

	@Override
	public void repeat() {
		// TODO Auto-generated method stub
		
	}


}
