package view.components.easteregg;

import interfaces.Drawable;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.Iterator;

public class Dots implements Drawable{

	private ArrayList<Point> points;
	
	private int widthOfDot = 10;
	private int heightOfDot = widthOfDot;
	
	public Dots() {
		points = new ArrayList<Point>(30);
	}
	
	
	public void draw(Graphics g) {
		
		Graphics2D gTemp = (Graphics2D)g.create();
		
		if(points.size() > 0) {
			Iterator<Point> iter = points.iterator();
			while(iter.hasNext()) {
				Point tmp = iter.next();
				RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
				renderHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
				gTemp.setRenderingHints(renderHints);
				gTemp.fillOval(tmp.x, tmp.y, widthOfDot, heightOfDot);
			}
		}		
		
	}
	
	public void addDot(Point p) {
		if(p != null) {
			points.add(p);
		}
	}

	public void clear() {
		points.clear();		
	}
	
}
