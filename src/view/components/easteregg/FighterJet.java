package view.components.easteregg;


import helpers.AnimationEngine;
import helpers.Settings;
import helpers.p;
import interfaces.Animatable;
import interfaces.Drawable;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.media.j3d.Texture2D;
import javax.swing.ImageIcon;

import controller.Actions;


public class FighterJet implements Drawable,Animatable{


	private int x = 0;
	private int y = 0;
	int prevX = x;
	int prevY = y;
	private int width;
	private int height;
	private boolean visible;

	private boolean forward;
	private boolean backward;
	private boolean left;
	private boolean right;


	private Image image;
	private ArrayList<Missile> missiles;

	//for antimation
	private AnimationEngine animationEngine;
	private boolean isAnimating = false;
	private int duration = 1000;
	private int repeatNum = 0;

	
	

	
	
	
	//curve 1
	private int px0 = 22;
	private int py0 = 186;	
	private int px1 = 11;
	private int py1 = 0;
	private int px2 = 689;
	private int py2 = 16;
	private int px3 = 643;	
	private int py3 = 191;

	private Point p0 = new Point(px0,py0);	
	private Point p1 = new Point(px1,py1);
	private Point p2 = new Point(px2,py2);
	private Point p3 = new Point(px3,py3);

	private BezierCurve curve1 = new BezierCurve(p0,p1,p2,p3);
	private ArrayList<Point> points0 = new ArrayList<Point>(50);
	private ArrayList<Point> points1 = new ArrayList<Point>(50);
	private ArrayList<Point> points2 = new ArrayList<Point>(50);
	private ArrayList<Point> points3 = new ArrayList<Point>(50);

	//end curve1

	
	

	
	
	
	//curve 2
	private int px4 = 626;
	private int py4 = 453;	
	private int px5 = 2;
	private int py5 = 487;
	private int px6 = 41;
	private int py6 = 166;

	private Point p4 = new Point(px4,py4);
	private Point p5 = new Point(px5,py5);
	private Point p6 = new Point(px6,py6);

	private BezierCurve curve2 = new BezierCurve(p3,p4,p5,p6);

	//end curve2	


	//curve 3
	private int px7 = 148;
	private int py7 = 13;	
	private int px8 = 665;
	private int py8 = 141;
	private int px9 = 597;
	private int py9 = 243;

	private Point p7 = new Point(px7,py7);
	private Point p8 = new Point(px8,py8);
	private Point p9 = new Point(px9,py9);

	private BezierCurve curve3 = new BezierCurve(p6,p7,p8,p9);

	//end curve3


	//curve 4
	private int px10 = 533;
	private int py10 = 388;	
	private int px11 = 118;
	private int py11 = 456;
	private int px12 = 58;
	private int py12 = 214;

	private Point p10 = new Point(px10,py10);
	private Point p11 = new Point(px11,py11);
	private Point p12 = new Point(px12,py12);

	private BezierCurve curve4 = new BezierCurve(p9,p10,p11,p12);

	//end curve4

	private BezierPath path = new BezierPath();
	

	/*
	private Point p0 = new Point(px0,py0);
	private Point p3 = new Point(px3,py3);
	private Point p1 = new Point(controlPointX1,py1);
	private Point p2 = new Point(px2,py2);



	private BezierCurve curve2 = new BezierCurve(p3,p5,p6,p7);
	private BezierCurve curve3 = new BezierCurve(p7,p8,p9,p10);
	private BezierCurve curve4 = new BezierCurve(p10,p11,p12,p13);*/


	public FighterJet() {	

		path.addCurve(curve1);
		path.addCurve(curve2);
		path.addCurve(curve3);
		path.addCurve(curve4);

		
		//URL imageURL = Missile.class.getResource("../../../resource/image/cr.gif");	
		//URL imageURL = Missile.class.getResource("../../../resource/image/bat.gif");
		//URL imageURL = Missile.class.getResource("../../../resource/image/bd.gif");
		//URL imageURL = Missile.class.getResource("../../../resource/image/tb.gif");
		//URL imageURL = Missile.class.getResource("../../../resource/image/bd1.gif");
		URL imageURL = Missile.class.getResource("../../../resource/image/ball.png");
		//URL imageURL = Missile.class.getResource("../../../resource/image/easter.gif");
		//java.net.URL imageURL = Missile.class.getResource("../../../resource/image/fighterjet.png"); 
		
		ImageIcon icon = new ImageIcon(imageURL);
		image = icon.getImage();
		/*
		try {
			//image = ImageIO.read(new File("src/resource/image/ball.png"));
			//image = ImageIO.read(new File("src/resource/image/cr.gif"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//URL imageURL = getClass().getResource("../../../resource/image/ball.png"); 
		
		
		/*
		java.net.URL imageURL = Missile.class.getResource("../../../resource/image/ball.png"); 
		try {
			image = ImageIO.read(imageURL	);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */

		this.width = image.getWidth(null);
		this.height = image.getHeight(null);



		missiles = new ArrayList<Missile>();
		
		visible = true;
		forward = false;
		backward = false;
		left = false;
		right = false;
		animationEngine = new AnimationEngine(duration,this);
		animationEngine.setPowerOfN(10);
		animationEngine.setEaseIn();
		
	}



	public void move() {

		if(forward){
			y += -1;
		}
		if(backward){
			y += 1;
		}
		if(left){
			x += -1;
		}
		if(right){
			x += 1;
		}

		if (x < 1) {
			x = 1;
		}

		if (y < 1) {
			y = 1;
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Image getImage() {
		return image;
	}

	public void fire() {
		missiles.add(new Missile(x + width, y + height/2));        
	}

	public ArrayList<Missile> getMissiles() {
		return missiles;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isVisible() {
		return visible;
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	public BezierPath getPath() {
		return path;
	}



	public void setPath(BezierPath path) {
		this.path = path;
	}



	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();

		if (key == KeyEvent.VK_SPACE) {
			System.out.println("FIRE");           
			fire();
		}

		if (key == KeyEvent.VK_LEFT) {
			left = true;
		}

		if (key == KeyEvent.VK_RIGHT) {
			right = true;
		}

		if (key == KeyEvent.VK_UP) {
			forward = true;
		}

		if (key == KeyEvent.VK_DOWN) {
			backward = true;
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			left = false;
		}

		if (key == KeyEvent.VK_RIGHT) {
			right = false;
		}

		if (key == KeyEvent.VK_UP) {
			forward = false;
		}

		if (key == KeyEvent.VK_DOWN) {
			backward = false;
		}
	}



	@Override
	public void draw(Graphics g) {

		if(isAnimating) {
			Graphics2D gTemp = (Graphics2D)g.create();	

			gTemp.drawImage(image,x,y,null);

			ArrayList<Missile> ms = getMissiles();
			for (int i = 0; i < ms.size(); i++) {
				Missile m = (Missile)ms.get(i);
				gTemp.drawImage(m.getImage(), m.getX(), m.getY(),null);
			}
		}

	}

	public void start() {
		path.getNumCurves();
		animationEngine.setRepeatNumTimes(path.getNumCurves());		
		animationEngine.start();
	}

	public void stop() {
		animationEngine.stop();
	}

	public void pause() {
		animationEngine.pause();
	}

	public void resume() {
		animationEngine.resume();
	}


	@Override
	public void begin() {
		isAnimating = true;

	}

	@Override
	public void repeat() {
		repeatNum++;	
		p.print("REPEAT!!!!");
		if(repeatNum >= path.getNumCurves()) {
			repeatNum = 0; 
		}
		
	}

	@Override
	public void end() {
		
		repeatNum = 0;
		isAnimating = false;

	}


	public Point getPointCubic(float t,BezierCurve curve) {


		int x = (int)(curve.getP0().x*(Math.pow(-t,3)+3*Math.pow(t,2)-3*t+1)+3*curve.getP1().x*t*(Math.pow(t,2)-2*t+1)
				+3*curve.getP2().x*Math.pow(t,2)*(1-t)+curve.getP3().x*Math.pow(t,3));

		int y =(int) (curve.getP0().y*(Math.pow(-t,3)+3*Math.pow(t,2)-3*t+1)
				+3*curve.getP1().y*t*(Math.pow(t,2)-2*t+1)
				+3*curve.getP2().y*Math.pow(t,2)*(1-t)+curve.getP3().y*Math.pow(t,3));




		return new Point(x,y);		
	}


	public Point getPointQuadratic(float t) {
		x = (int)((((1-t)*(1-t)) * p0.x) + (2 * t * (1 - t) * p1.x) + ((t * t) * p2.x));
		y = (int)((((1-t)*(1-t)) * p0.y) + (2 * t * (1 - t) * p1.y) + ((t * t) * p2.y));

		return new Point(x,y);
	}

	@Override
	public void animationEvent(float fraction) {

		prevX = x;
		prevY = y;
		Point cubic = null;
		
		cubic = getPointCubic(fraction,path.getCurves().get(repeatNum));
		/*if(repeatNum == 0) {
			cubic = getPointCubic(fraction,path.getCurves().get(0));
			points0.add(cubic);			
		} else if(repeatNum == 1){
			cubic = getPointCubic(fraction,path.getCurves().get(1));
			points1.add(cubic);			
		} else if(repeatNum == 2){
			cubic = getPointCubic(fraction,path.getCurves().get(2));
			points2.add(cubic);			
		} else if(repeatNum == 3){
			cubic = getPointCubic(fraction,path.getCurves().get(3));
			points3.add(cubic);			
		}*/
		
		
		
		
		
		
		
		

		//Point quadratic = getPointQuadratic(fraction);
		if(cubic != null) {
			 
			x = cubic.x;
			y = cubic.y;
		}

		//x = quadratic.x; 
		//y = quadratic.y; 

		//Actions.view.repaint();
		Actions.view.repaint(prevX,prevY,width,height);
		Actions.view.repaint(x,y,width,height);

	}



	


}
