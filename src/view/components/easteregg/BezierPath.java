package view.components.easteregg;

import java.awt.Point;
import java.util.ArrayList;

public class BezierPath {

	private ArrayList<BezierCurve> curves;

	private ArrayList<Point> points;
	private boolean firstCurve = true;
	private int numCurves = 0; 

	public BezierPath() {
		curves = new ArrayList<BezierCurve>(5);
		points = new ArrayList<Point>(12);
	}

	public void addCurve(BezierCurve curve) {
		if(curve != null) {
			curves.add(curve);
			numCurves++;
		}
	}

	public ArrayList<BezierCurve>  getCurves() {
		return curves;
	}

	
	public void addPoint(Point p) {
		if(p != null) {
			points.add(p);
		}
	}
	public int getNumCurves() {
		return this.numCurves;
	}

	public void clear() {
		points.clear();
		curves.clear();
		numCurves = 0;
	}

	public void generatePath() {

		int len = points.size();

		int numCurves = (int)(len/3);
		int j = 0;
		for (int i = 0; i < len; i = i+3 ) {
			j++;
			if(j <= numCurves) {
				if(firstCurve) {
					addCurve(new BezierCurve(points.get(i), points.get(i+1), points.get(i+2), points.get(i+3)));
					firstCurve = false;
				} else {
					addCurve(new BezierCurve(points.get(i), points.get(i+1), points.get(i+2), points.get(i+3)));					
				}
			}		
		}




	}

}
