package view.components.easteregg;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Missile {

    private int x, y;
    private BufferedImage image;
    boolean visible;
    private int width, height;

    private final int MISSILE_SPEED = 2 ;

    public Missile(int x, int y) {
    	java.net.URL imageURL = Missile.class.getResource("../../../resource/image/missile.png");
        try {
			image = ImageIO.read(imageURL);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
        visible = true;
        width = image.getWidth(null);
        height = image.getHeight(null);
        this.x = x;
        this.y = y;
    }


    public Image getImage() {
        return image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    public void move(int x,int y) {
        this.x= x;
        this.y= y;
    }
}
