package view.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import model.XmlTaskModel;

import helpers.Settings;
import interfaces.Drawable;

public class ControlPanelHeader implements Drawable{

	private int width;
	private int height;

	private int calendarHeaderWidth = Settings.getHeaderWidth();
	private int calendarHeaderHeight = Settings.getHeaderHeight();

	CustomButton schoolButton;
	CustomButton workButton;
	CustomButton homeButton;
	CustomButton otherButton;
	CustomButton lowPriorityButton;
	CustomButton highPriorityButton;

	private int posXOfRectangle;
	private int posYOfRectangle;
	private int widthOfRectangle;
	private int heightOfRectangle;

	private int posXOfControlPanel;
	private int posYOfControlPanel;

	private int buttonWidth = 120;
	private int buttonHeight = 30;

	private boolean isHomeClicked = false;
	private boolean isWorkClicked = false;
	private boolean isSchoolClicked = false;
	private boolean isOtherClicked = false;
	private boolean isLowPriorityClicked = false;
	private boolean isHighPriorityClicked = false;


	public ControlPanelHeader(int width, int height) {
		this.width = width;
		this.height = height;

		posXOfControlPanel = Settings.getHeaderWidth()/3;
		posYOfControlPanel = Settings.getHeaderHeight()/3 + (30);
		widthOfRectangle = (Settings.getHeaderWidth()/2);
		heightOfRectangle = (Settings.getHeaderHeight() * 2);


		//Category buttons
		int firstRowButtons = posYOfControlPanel + 40;
		int schoolPosX = posXOfControlPanel + 30;
		schoolButton = new CustomButton(buttonWidth, buttonHeight, schoolPosX, firstRowButtons, "School");
		schoolButton.setColor(XmlTaskModel.getCategories().getColor("School"));

		int workPosX = schoolPosX + 170;
		workButton = new CustomButton(buttonWidth, buttonHeight, workPosX, firstRowButtons, "Work");
		workButton.setColor(XmlTaskModel.getCategories().getColor("Work"));

		int homePosX = workPosX + 340;
		homeButton = new CustomButton(buttonWidth, buttonHeight, homePosX, firstRowButtons, "Home");
		homeButton.setColor(XmlTaskModel.getCategories().getColor("Home"));

		int secondRowButtons = posYOfControlPanel + 120;
		otherButton = new CustomButton(buttonWidth, buttonHeight, schoolPosX, secondRowButtons, "Other");
		otherButton.setColor(XmlTaskModel.getCategories().getColor("Other"));

		lowPriorityButton = new CustomButton(buttonWidth, buttonHeight, workPosX, secondRowButtons, "Low Priority");
		Color prio = new Color(255,211,153);
		lowPriorityButton.setColor(prio);
		
		highPriorityButton = new CustomButton(buttonWidth, buttonHeight, homePosX, secondRowButtons, "High Priority");
		highPriorityButton.setColor(prio);
	}


	@Override
	public void draw(Graphics g) {

		Graphics2D gTemp = (Graphics2D)g.create();
		this.calendarHeaderWidth = Settings.getHeaderWidth();
		this.calendarHeaderHeight = Settings.getHeaderHeight();

		Color rectColor = new Color(
				Color.DARK_GRAY.getRed(),
				Color.DARK_GRAY.getGreen(),
				Color.DARK_GRAY.getBlue(),
				180);
		gTemp.setColor(rectColor);
		posXOfControlPanel = Settings.getHeaderWidth()/2 - 190;
		posYOfControlPanel = Settings.getHeaderHeight()/3 + (30);


		gTemp.fillRoundRect(posXOfControlPanel, posYOfControlPanel, widthOfRectangle, heightOfRectangle, 45, 45);
		gTemp.setColor(Color.GREEN);


		gTemp.drawString("What to highlight?", posXOfControlPanel + 185, posYOfControlPanel + 20);

		//Category buttons
		schoolButton.setX(posXOfControlPanel + 20);
		schoolButton.setY(posYOfControlPanel + 40);
		schoolButton.setButtonTextPosX(posXOfControlPanel + 60);
		schoolButton.setButtonTextPosY(posYOfControlPanel + 60);
		schoolButton.draw(gTemp);

		workButton.setX(posXOfControlPanel + 190);
		workButton.setY(posYOfControlPanel + 40);
		workButton.setButtonTextPosX(posXOfControlPanel + 230);
		workButton.setButtonTextPosY(posYOfControlPanel + 60);
		workButton.draw(gTemp);

		homeButton.setX(posXOfControlPanel + 360);
		homeButton.setY(posYOfControlPanel + 40);
		homeButton.setButtonTextPosX(posXOfControlPanel + 400);
		homeButton.setButtonTextPosY(posYOfControlPanel + 60);
		homeButton.draw(gTemp);

		otherButton.setX(posXOfControlPanel + 20);
		otherButton.setY(posYOfControlPanel + 120);
		otherButton.setButtonTextPosX(posXOfControlPanel + 60);
		otherButton.setButtonTextPosY(posYOfControlPanel + 140);
		otherButton.draw(gTemp);

		lowPriorityButton.setX(posXOfControlPanel + 190);
		lowPriorityButton.setY(posYOfControlPanel + 120);
		lowPriorityButton.setButtonTextPosX(posXOfControlPanel + 205);
		lowPriorityButton.setButtonTextPosY(posYOfControlPanel + 140);
		lowPriorityButton.draw(gTemp);

		highPriorityButton.setX(posXOfControlPanel + 360);
		highPriorityButton.setY(posYOfControlPanel + 120);
		highPriorityButton.setButtonTextPosX(posXOfControlPanel + 375);
		highPriorityButton.setButtonTextPosY(posYOfControlPanel + 140);
		highPriorityButton.draw(gTemp);
	}

	/*public boolean isInsideControlPanelButton(int mouseX, int mouseY) {
		boolean res = false;
		
		if(mouseX >= calendarHeaderWidth/2 && mouseX <= ((calendarHeaderWidth/2) + calendarHeaderWidth) && mouseY >= calendarHeaderHeight/3 && mouseY <= (calendarHeaderHeight/3 + calendarHeaderHeight)) {
			res = true;
			System.out.println("isInsideControlPanelButton: " + res);
		}
		return res;
	}*/
	
	public boolean isInsideControlPanel(int mouseX, int mouseY) {
		boolean res = false;
		if(mouseX >= posXOfControlPanel && mouseX <= (posXOfControlPanel+widthOfRectangle) && mouseY >= posYOfControlPanel && mouseY <= (posYOfControlPanel+heightOfRectangle)) {
			res = true;

		}
		return res;
	}


	public int getControlPanelWidth() {
		return width;
	}

	public void setControlPanelWidth(int i) {
		width = i;
	}

	public int getControlPanelHeight() {
		return height;
	}

	public void setControlPanelHeight(int i) {
		height = i;
	}


	public CustomButton getSchoolButton() {
		return schoolButton;
	}


	public void setSchoolButton(CustomButton schoolButton) {
		this.schoolButton = schoolButton;
	}


	public CustomButton getWorkButton() {
		return workButton;
	}


	public void setWorkButton(CustomButton workButton) {
		this.workButton = workButton;
	}


	public CustomButton getHomeButton() {
		return homeButton;
	}


	public void setHomeButton(CustomButton homeButton) {
		this.homeButton = homeButton;
	}


	public CustomButton getOtherButton() {
		return otherButton;
	}


	public void setOtherButton(CustomButton otherButton) {
		this.otherButton = otherButton;
	}


	public CustomButton getLowPriorityButton() {
		return lowPriorityButton;
	}


	public void setLowPriorityButton(CustomButton lowPriorityButton) {
		this.lowPriorityButton = lowPriorityButton;
	}


	public CustomButton getHighPriorityButton() {
		return highPriorityButton;
	}


	public void setHighPriorityButton(CustomButton highPriorityButton) {
		this.highPriorityButton = highPriorityButton;
	}


	public boolean isHomeClicked() {
		return isHomeClicked;
	}


	public void setHomeClicked(boolean isHomeClicked) {
		this.isHomeClicked = isHomeClicked;
	}


	public boolean isWorkClicked() {
		return isWorkClicked;
	}


	public void setWorkClicked(boolean isWorkClicked) {
		this.isWorkClicked = isWorkClicked;
	}


	public boolean isSchoolClicked() {
		return isSchoolClicked;
	}


	public void setSchoolClicked(boolean isSchoolClicked) {
		this.isSchoolClicked = isSchoolClicked;
	}


	public boolean isOtherClicked() {
		return isOtherClicked;
	}


	public void setOtherClicked(boolean isOtherClicked) {
		this.isOtherClicked = isOtherClicked;
	}


	public boolean isLowPriorityClicked() {
		return isLowPriorityClicked;
	}


	public void setLowPriorityClicked(boolean isLowPriorityClicked) {
		this.isLowPriorityClicked = isLowPriorityClicked;
	}


	public boolean isHighPriorityClicked() {
		return isHighPriorityClicked;
	}


	public void setHighPriorityClicked(boolean isHighPriorityClicked) {
		this.isHighPriorityClicked = isHighPriorityClicked;
	}
	
	


}
