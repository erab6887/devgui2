package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalendarCategoryCreator extends JDialog implements ActionListener {
	
	private JLabel categoryLabel, chosenColorLabel;
	private JTextField categoryField;
	private JButton colorChooserButton, saveButton;
	private Color categoryColor = Color.CYAN;
	
	public CalendarCategoryCreator(JDialog owner, String title, boolean modality) {
		
		super(owner, title, modality);
		this.setMinimumSize(new Dimension(250, 230));
		this.setPreferredSize(new Dimension(250, 230));
		this.setResizable(false);
		
		JPanel panel = new JPanel(new GridBagLayout());
		
		GridBagConstraints labelConstraints = new GridBagConstraints();
		labelConstraints.fill = GridBagConstraints.BOTH;
		labelConstraints.gridwidth = 1;
		labelConstraints.gridheight = 1;
		labelConstraints.weightx = 0.9;
		labelConstraints.weighty = 0.05; 
		labelConstraints.insets = new Insets(0,20,0,20);
		labelConstraints.anchor = GridBagConstraints.LINE_START;
		
		GridBagConstraints inputConstraints = new GridBagConstraints();
		inputConstraints.fill = GridBagConstraints.BOTH;
		inputConstraints.gridwidth = 1;
		inputConstraints.gridheight = 1;
		inputConstraints.weightx = 0.9;
		inputConstraints.weighty = 0.05; 
		inputConstraints.insets = new Insets(0,20,15,20);
		inputConstraints.anchor = GridBagConstraints.LINE_START;
		
		categoryLabel = new JLabel("Category name:");
		categoryField = new JTextField();
		colorChooserButton = new JButton("Choose category color!");
		colorChooserButton.addActionListener(this);
		chosenColorLabel = new JLabel("Chosen color:");
		saveButton = new JButton("Save");
		
		labelConstraints.gridx = 0;
		labelConstraints.gridy = 0;
		panel.add(categoryLabel, labelConstraints);
		
		inputConstraints.gridx = 0;
		inputConstraints.gridy = 1;
		panel.add(categoryField, inputConstraints);
		
		inputConstraints.gridx = 0;
		inputConstraints.gridy = 2;
		panel.add(chosenColorLabel, inputConstraints);
		
		inputConstraints.gridx = 0;
		inputConstraints.gridy = 3;
		panel.add(colorChooserButton, inputConstraints);
		
		inputConstraints.gridx = 0;
		inputConstraints.gridy = 4;
		panel.add(saveButton, inputConstraints);
		
		this.add(panel);
		
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		categoryColor = JColorChooser.showDialog(this, "Category color", categoryColor);
		this.repaint();
	}
	
	public JButton getSaveButton() {
		return saveButton;
	}
	
	public Color getColor() {
		return categoryColor;
	}
	
	public String getCategoryField() {
		return categoryField.getText();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D gTemp = (Graphics2D)g.create();
		gTemp.setColor(categoryColor);
		gTemp.fillRect(120, 92, 25, 25);
		gTemp.setColor(Color.BLACK);
		gTemp.drawRect(120, 92, 25, 25);
		gTemp.dispose();
	}
}
