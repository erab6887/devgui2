package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import interfaces.Drawable;

import javax.swing.JComponent;

import view.components.CustomButton;

public class CustomGlassPane extends JComponent {

	private int x;
	private int y;
	private double height;
	private double width;
	private CustomButton closeButton;
	
	public CustomGlassPane(int x, int y, double height, double width) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		closeButton = new CustomButton((int) (width * 0.15), (int) (height * 0.08), x + 10, (int) (y + height - 10), "Close");
	}
	
	@Override
	public void paint(Graphics g) {	
		super.paintComponent(g);
		System.out.println("GlassPanes paintComponent!!!!");
		
		Graphics2D gTemp = (Graphics2D)g.create();
		
	    Color alphaWhite = new Color(1.0f, 1.0f, 1.0f, 0.75f);
	    gTemp.setColor(alphaWhite);
	    gTemp.fillRoundRect(x, y, (int) width, (int) height, 10, 10);
	    
	    Color alphaGrey = new Color(0.9f, 0.9f, 0.9f, 0.85f);
	    gTemp.setColor(alphaGrey);
	    int rectHeight = (int) (height * 0.25);
	    gTemp.fillRoundRect(x + 10, y + 10, (int) (width - 20), rectHeight, 10, 10);
	    //Text for the above rectangle
	    Font titleFont = new Font("Arial", Font.BOLD, 15);
	    Font descriptionFont = new Font("Arial", Font.PLAIN, 13);
	    
	    String titleCreateTask = ("Creating new tasks");
	    String createTaskDescription = ("Click and drag with the mouse in one of the weekdays to create a new task.");
	   
	    gTemp.setColor(Color.BLACK);
	    gTemp.setFont(titleFont);
	    gTemp.drawString(titleCreateTask, x + 20, y + 30);
	    gTemp.setFont(descriptionFont);
	    gTemp.drawString(createTaskDescription, x + 20, y + 60);
	    
	    
	    Color alphaGrey2 = new Color(0.9f, 0.9f, 0.9f, 0.95f);
	    gTemp.setColor(alphaGrey2);
	    gTemp.fillRoundRect(x + 10, y + rectHeight + 20, (int) (width - 20), rectHeight, 10, 10);
	    //Text for the above rectangle
	    String titleMovingTask = ("Moving tasks");
	    String movingTaskDescription = ("Click and drag a task in order to reschedule it some other time and/or day.");
	 
	    gTemp.setColor(Color.BLACK);
	    gTemp.setFont(titleFont);
	    gTemp.drawString(titleMovingTask, x + 20, y + rectHeight + 40);
	    gTemp.setFont(descriptionFont);
	    gTemp.drawString(movingTaskDescription, x + 20, y + rectHeight + 70);
	    
	    
	    Color alphaGrey3 = new Color(0.9f, 0.9f, 0.9f, 0.85f);
	    gTemp.setColor(alphaGrey3);
	    gTemp.fillRoundRect(x + 10, y + (rectHeight*2) + 30, (int) (width - 20), rectHeight, 10, 10);
	    //Text for the above rectangle
	    String titleEditTask = ("Edit tasks");
	    String editTaskDescription = ("Double click a task to be able to edit its information.");
	    
	    gTemp.setColor(Color.BLACK);
	    gTemp.setFont(titleFont);
	    gTemp.drawString(titleEditTask, x + 20, y + (rectHeight*2) + 50);
	    gTemp.setFont(descriptionFont);
	    gTemp.drawString(editTaskDescription, x + 20, y + (rectHeight*2) + 80);
	    

	    Color alphaButton = new Color(0.7f, 0.7f, 0.7f, 0.75f);
	    gTemp.setColor(alphaButton);
	    closeButton.setX(x + 10);
		closeButton.setY((int) (y + height - 40));
		closeButton.setButtonTextPosX(x + 30);
		closeButton.setButtonTextPosY((int) (y + height - 23));
		closeButton.draw(gTemp);
		
	}

	public CustomButton getCloseButton() {
		return closeButton;
	}
	
}
