package helpers;

public class Settings {
	
	private static int frameWidth = 1000;
	private static int frameHeight = 720;
	
	private static int headerHeight = (int)(frameHeight*0.15);
	private static int headerWidth = frameWidth;
	
	private static int weekCardPosY = headerHeight;
	private static int weekCardWidth = frameWidth;
	private static int weekCardHeight = (frameHeight - headerHeight);
	
	private static int paddingBetweenDayCards = 2;
	
	public final static int MONDAY_POS_X = 60; 
	
	private static int dayCardPadding = (int)(frameWidth * 0.06);//60;
	//private static int dayCardWidth =  ((weekCardWidth-dayCardPadding-22)/7);
	private static int dayCardWidth =  ((weekCardWidth-MONDAY_POS_X-10)/7);
	private static int dayCardHeight = weekCardHeight-20;
	private static int dayCardPosY = weekCardPosY+10;
	
	private static boolean resizeMousePointer = false;
	private static boolean addEventToModel = false;
	private static int eventCardThatToBeAddedToModel = -1;
	private static int eventCardThatIsChanging = -1;
	private static boolean pressedOnEventCard = false;
	
	
	
	
	public synchronized static int getFrameWidth() {
		return frameWidth;
	}
	public synchronized static void setFrameWidth(int frameWidth) {
		Settings.frameWidth = frameWidth;
		Settings.headerHeight = (int)(frameHeight*0.15);
		Settings.weekCardPosY = headerHeight;
		Settings.headerWidth = Settings.frameWidth;
		Settings.weekCardHeight = (frameHeight - headerHeight);
		Settings.weekCardWidth = Settings.frameWidth;
		Settings.dayCardPadding = (int)(frameWidth * 0.06);
		Settings.dayCardPosY = weekCardPosY+10;
		Settings.dayCardHeight = weekCardHeight-20;
		Settings.dayCardWidth = ((weekCardWidth-dayCardPadding-22)/7);
	}
	public synchronized static int getFrameHeight() {
		return frameHeight;
	}
	public synchronized static void setFrameHeight(int frameHeight) {
		Settings.frameHeight = frameHeight;
	}	
	
	public synchronized static boolean isResizeMousePointer() {
		return resizeMousePointer;
	}
	public synchronized static void setResizeMousePointer(boolean resizeMousePointer) {
		Settings.resizeMousePointer = resizeMousePointer;
	}
	public synchronized static int getHeaderHeight() {
		return headerHeight;
	}
	public synchronized static void setHeaderHeight(int headerHeight) {
		Settings.headerHeight = headerHeight;
	}
	public synchronized static int getWeekCardHeight() {
		return weekCardHeight;
	}
	public synchronized static void setWeekCardHeight(int weekCardHeight) {
		Settings.weekCardHeight = weekCardHeight;
	}
	

	public static int getDayCardHeight() {
		return dayCardHeight;
	}
	public static void setDayCardHeight(int dayCardHeight) {
		Settings.dayCardHeight = dayCardHeight;
	}
	public static int getDayCardWidth() {
		return dayCardWidth;
	}
	public static void setDayCardWidth(int dayCardWidth) {
		Settings.dayCardWidth = dayCardWidth;
	}
	public static int getDayCardPadding() {
		return dayCardPadding;
	}
	public static void setDayCardPadding(int dayCardPadding) {
		Settings.dayCardPadding = dayCardPadding;
	}
	public static int getPaddingBetweenDayCards() {
		return paddingBetweenDayCards;
	}
	public static void setPaddingBetweenDayCards(int paddingBetweenDayCards) {
		Settings.paddingBetweenDayCards = paddingBetweenDayCards;
	}
	public static int getHeaderWidth() {
		return headerWidth;
	}
	public static void setHeaderWidth(int headerWidth) {
		Settings.headerWidth = headerWidth;
	}
	public static int getWeekCardPosY() {
		return weekCardPosY;
	}
	public static void setWeekCardPosY(int weekCardPosY) {
		Settings.weekCardPosY = weekCardPosY;
	}
	public static int getDayCardYpos() {
		return dayCardPosY;
	}
	public static void setDayCardYpos(int dayCardYpos) {
		Settings.dayCardPosY = dayCardYpos;
	}
	public static boolean isAddEventToModel() {
		return addEventToModel;
	}
	public static void setAddEventToModel(boolean addEventToModel) {
		Settings.addEventToModel = addEventToModel;
	}
	public static int getEventCardThatToBeAddedToModel() {
		return eventCardThatToBeAddedToModel;
	}
	public static void setEventCardThatToBeAddedToModel(int eventCardThatIsChanging) {
		Settings.eventCardThatToBeAddedToModel = eventCardThatIsChanging;
	}
	public static int getEventCardThatIsChanging() {
		return eventCardThatIsChanging;
	}
	public static void setEventCardThatIsChanging(int eventCardThatIsChanging) {
		Settings.eventCardThatIsChanging = eventCardThatIsChanging;
	}
	public static boolean isPressedOnEventCard() {
		return pressedOnEventCard;
	}
	public static void setPressedOnEventCard(boolean pressedOnEventCard) {
		Settings.pressedOnEventCard = pressedOnEventCard;
	}
	
	
	
}
