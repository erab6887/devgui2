package helpers;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

public class CalendarHelper {

	
	//will always be the week number of the week when the applications was started
	private static int thisWeek;

	//will be the week number that changes when the user change week in the application.
	private static int weekNumber;


	//the day of the month that the current week begins with
	private static int startDay;
	//the day of the month that the current week ends with
	private static int endDay;

	//the month the week begins with
	private static int startMonth;
	//the month the week ends with
	private static int endMonth;

	//the year the week begins with
	private static int startYear;
	//the year the week ends with
	private static int endYear;

	public static int monday;
	public static int tuesday;
	public static int wednesday;
	public static int thursday;
	public static int friday;
	public static int saturday;
	public static int sunday;




	private static int year;
	private static int month;	
	public static LocalDate date;

	public static int  getWeekNumber() {
		return weekNumber;
	}	

	public static int getYear() {
		return year;
	}

	public static int getMonth() {
		return month;
	}

	public static LocalDate getDate() {		
		
		return CalendarHelper.date;
	}

	public static void setDate(LocalDate date) {
		CalendarHelper.date = date;	
		weekNumber = date.getWeekOfWeekyear();
		month =  date.getMonthOfYear();
		year = date.getWeekyear();
		startDay = date.withDayOfWeek(DateTimeConstants.MONDAY).getDayOfMonth();
		endDay = date.withDayOfWeek(DateTimeConstants.SUNDAY).getDayOfMonth();
		startMonth = date.withDayOfWeek(DateTimeConstants.MONDAY).getMonthOfYear();
		endMonth = date.withDayOfWeek(DateTimeConstants.SUNDAY).getMonthOfYear();
		startYear = date.withDayOfWeek(DateTimeConstants.MONDAY).getYear();
		endYear = date.withDayOfWeek(DateTimeConstants.SUNDAY).getYear();
		monday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.MONDAY).getDayOfMonth();
		tuesday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.TUESDAY).getDayOfMonth();
		wednesday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.WEDNESDAY).getDayOfMonth();
		thursday =  CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.THURSDAY).getDayOfMonth();
		friday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.FRIDAY).getDayOfMonth();
		saturday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.SATURDAY).getDayOfMonth();
		sunday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.SUNDAY).getDayOfMonth();
	}

	public static void minusOneWeek(){
		System.out.println("ллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллл");
		date = date.minusWeeks(1);
		weekNumber = date.getWeekOfWeekyear();
		month =  date.getMonthOfYear();
		year = date.getWeekyear();
		startDay = date.withDayOfWeek(DateTimeConstants.MONDAY).getDayOfMonth();
		endDay = date.withDayOfWeek(DateTimeConstants.SUNDAY).getDayOfMonth();
		startMonth = date.withDayOfWeek(DateTimeConstants.MONDAY).getMonthOfYear();
		endMonth = date.withDayOfWeek(DateTimeConstants.SUNDAY).getMonthOfYear();
		startYear = date.withDayOfWeek(DateTimeConstants.MONDAY).getYear();
		endYear = date.withDayOfWeek(DateTimeConstants.SUNDAY).getYear();
		monday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.MONDAY).getDayOfMonth();
		tuesday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.TUESDAY).getDayOfMonth();
		wednesday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.WEDNESDAY).getDayOfMonth();
		thursday =  CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.THURSDAY).getDayOfMonth();
		friday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.FRIDAY).getDayOfMonth();
		saturday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.SATURDAY).getDayOfMonth();
		sunday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.SUNDAY).getDayOfMonth();
	} 

	public static void plusOneWeek(){
		date = date.plusWeeks(1);
		weekNumber = date.getWeekOfWeekyear();
		month =  date.getMonthOfYear();
		year = date.getWeekyear();
		startDay = date.withDayOfWeek(DateTimeConstants.MONDAY).getDayOfMonth();
		endDay = date.withDayOfWeek(DateTimeConstants.SUNDAY).getDayOfMonth();
		startMonth = date.withDayOfWeek(DateTimeConstants.MONDAY).getMonthOfYear();
		endMonth = date.withDayOfWeek(DateTimeConstants.SUNDAY).getMonthOfYear();
		startYear = date.withDayOfWeek(DateTimeConstants.MONDAY).getYear();
		endYear = date.withDayOfWeek(DateTimeConstants.SUNDAY).getYear();
		monday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.MONDAY).getDayOfMonth();
		tuesday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.TUESDAY).getDayOfMonth();
		wednesday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.WEDNESDAY).getDayOfMonth();
		thursday =  CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.THURSDAY).getDayOfMonth();
		friday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.FRIDAY).getDayOfMonth();
		saturday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.SATURDAY).getDayOfMonth();
		sunday = CalendarHelper.getDate().withDayOfWeek(DateTimeConstants.SUNDAY).getDayOfMonth();
	} 



	public static String getMonthAsString(int month) {
		String res = "";
		switch(month) {
		case 1 : 	res = "January";		
		break;
		case 2 : 	res = "February";
		break;
		case 3 : 	res = "March";
		break;
		case 4 : 	res = "April";
		break;
		case 5 :	res = "May";
		break;
		case 6 :	res = "June";
		break;
		case 7 :	res = "July";
		break;
		case 8 : 	res = "August";
		break;
		case 9 : 	res = "September";
		break;	
		case 10: 	res = "October";
		break;
		case 11: 	res = "November";
		break;
		case 12: 	res = "December";
		break;
		default: 	res = "Invalid month";
		break;
		}
		return res;
	}
	
	public static String getWeekDayAsString(int dayOfWeek) {
		String res = "";
		switch(dayOfWeek) {
		case 1 : 	res = "Monday";		
		break;
		case 2 : 	res = "Tuesday";
		break;
		case 3 : 	res = "Wednesday";
		break;
		case 4 : 	res = "Thursday";
		break;
		case 5 :	res = "Friday";
		break;
		case 6 :	res = "Saturday";
		break;
		case 7 :	res = "Sunday";
		break;	
		default: 	res = "Invalid day";
		break;
		}
		return res;
	}


	/**
	 * 
	 * @param weekNumber, the week number which the date belongs to
	 * @param day, should be one of the static field declared in java.util.Calendar. e.g. Calendar.MONDAY
	 * @return
	 */

	public static LocalDate getDate(int day,int weekNumber,int year) {		
		//LocalDate now = new LocalDate().withWeekOfWeekyear(weekNumber).withYear(year);		
		date.withWeekOfWeekyear(weekNumber).withYear(year);

		return date.withDayOfWeek(day);		

	}

	public static int getThisWeek() {
		return thisWeek;
	}

	public static void setThisWeek(int thisWeek) {
		CalendarHelper.thisWeek = thisWeek;		
	}

	public static int getStartDay() {
		return startDay;
	}

	public static int getEndDay() {
		return endDay;
	}

	public static int getStartMonth() {
		return startMonth;
	}

	public static int getEndMonth() {
		return endMonth;
	}

	public static int getStartYear() {
		return startYear;
	}

	public static int getEndYear() {
		return endYear;
	}
	
	
	
	







}
